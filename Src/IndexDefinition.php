<?php
namespace EoneoPay\DatabaseRepos;
use EoneoPay\DatabaseRepos\Exceptions\UnsupportedIndexException;

class IndexDefinition implements \JsonSerializable
{
    private $sHashKey;
    private $sSortKey;
    private $sIndexName;
    private $sProjectionType;
    private $bIsGlobal;
    private $bIsPrimaryKey;
    private $iPriority;

    /**
     * Serialize this object to a JSON-representation
     * 
     * @return  string JSON-representation of this object
     */
    public function jsonSerialize(): array 
    {
        return [
            'sHashKey'          => $this->sHashKey,
            'sSortKey'          => $this->sSortKey,
            'sIndexName'        => $this->sIndexName,
            'sProjectionType'   => $this->sProjectionType,
            'bIsGlobal'         => $this->bIsGlobal,
            'bIsPrimaryKey'     => $this->bIsPrimaryKey,
            'iPriority'         => $this->iPriority,
        ];
    }

    public function __construct(string $sHashKey,
                                ?string $sSortKey = null,
                                string $sIndexName,
                                string $sProjectionType,
                                bool $bIsGlobal,
                                bool $bIsPrimaryKey,
                                int $iPriority)
    {
        if ($sProjectionType != 'ALL') {
            throw new UnsupportedIndexException('Other projection type other than ALL is currently unsupported');
        }

        if (!$bIsGlobal) {
            throw new UnsupportedIndexException('Local indexes are not supported');
        }

        if (strlen($sIndexName) < 3
            || strlen($sIndexName) > 255) {
            throw new UnsupportedIndexException('Name for index must be at least 3 characters and at most 255.');
        }

        if (preg_match('/[^\-\.\_a-z0-9]/i', $sIndexName)) {
            throw new UnsupportedIndexException('Index name can only contain the following characters: _-. A-Z and 0-9');
        }

        $this->sHashKey = $sHashKey;
        $this->sSortKey = $sSortKey;
        $this->sIndexName = $sIndexName;
        $this->sProjectionType = $sProjectionType;
        $this->bIsGlobal = $bIsGlobal;
        $this->bIsPrimaryKey = $bIsPrimaryKey;
        $this->iPriority = $iPriority;
    }

    public function getHashKey(): string
    {
        return $this->sHashKey;
    }

    public function getSortKey(): ?string
    {
        return $this->sSortKey;
    }

    public function isPrimaryKey(): bool
    {
        return $this->bIsPrimaryKey;
    }

    public function getProjectionType(): string
    {
        return $this->sProjectionType;
    }

    public function getPriority(): int
    {
        return $this->iPriority;
    }

    public function isGlobal(): bool
    {
        return $this->bIsGlobal;
    }

    public function getIndexName(): string 
    {
        return $this->sIndexName;
    }

    public function isSignificantlyDifferentThan(IndexDefinition $oIndexDefinition)
    {
        return $oIndexDefinition->getHashKey() != $this->getHashKey()
            || $oIndexDefinition->getSortKey() != $this->getSortKey()
            || $oIndexDefinition->isGlobal() != $this->isGlobal()
            || $oIndexDefinition->getProjectionType() != $this->getProjectionType();
    }
}
