<?php
namespace EoneoPay\DatabaseRepos;
use EoneoPay\DatabaseRepos\Exceptions\{QueryException,InvalidQueryFormatException};

abstract class Criteria
{
    private $aFormattedCriteria = [];
    private $aNestedCriteria = [];
    private $oTableDefinition = null;

    private $sFilterExpression = '';
    private $sKeyConditionExpression = '';
    private $aAttributeNames = [];
    private $aAttributeValues = [];
    private $aFilterExpressionItems = [];
    private $aKeyConditionItems = [];
    private $oUsedIndex = null; 

    private $bEnableIndexes = true;

    private $aOperatorAliases = [];

    abstract public function getLogicalOperator();

    public function __construct(array $aUnprocessedCriteria)
    {
        $this->addCriteria($aUnprocessedCriteria);
    }

    /**
     * Add criteria to this object and formats them internally. This method sets the $aFormattedCriteria 
     * property on this object.
     * 
     * @param array $aUnprocessedCriteria  An array specified by the user on what to filter on. This array
     *                                     can be specified in different ways, like so:
     *                                     [
     *                                         [$sAttributeName, $sOperator, $sAttributeValue],
     *                                         ['name', '=', 'Bob'],
     *                                         ...
     *                                     ]
     *                                     OR
     *                                     [
     *                                         $sAttributeName => $sAttributeValue   //operator will be set as =
     *                                     ]
     *                                     OR
     *                                     [
     *                                         new ORCriteria(['name', '=', 'Alice']),
     *                                         ...
     *                                     ]
     *                                     OR
     *                                     [
     *                                         [$sAttributeName, $sOperator, $sAttributeValue],
     *                                         [$sAttributeName => $sAttributeValue],
     *                                         new ORCriteria([
     *                                             'name', '=', 'Alice',
     *                                             [$sAttributeName => $sAttributeValue],
     *                                         ]),
     *                                         ...
     *                                     ]
     *                                     As you can see, it is possible to mix different styles.
     * 
     * 
     */
    public function addCriteria(array $aUnprocessedCriteria): void
    {
        $aFormattedCriteria = [];
        foreach ($aUnprocessedCriteria as $sCriterionKey => $mCriterionItem) {
            if (is_array($mCriterionItem)
                && count($mCriterionItem) == 3) {
                $aFormattedCriteria[] = array_values($mCriterionItem);
            } else if (is_scalar($mCriterionItem)) {
                $aFormattedCriteria[] = [$sCriterionKey, '=', $mCriterionItem];
            } else if ($mCriterionItem instanceof Criteria) {
                $this->aNestedCriteria[] = $mCriterionItem;
            } else if (is_array($mCriterionItem)
                && count($mCriterionItem) == 1
                && is_scalar(reset($mCriterionItem))) {
                $aFormattedCriteria[] = [key($mCriterionItem), '=', reset($mCriterionItem)]; 
            } else {
                throw new InvalidQueryFormatException(sprintf("The format for the query is invalid for AttributeName '%s'", $sCriterionKey));
            }
        }
        $this->aFormattedCriteria = array_merge($aFormattedCriteria, $this->aFormattedCriteria);
    }



    /**
     * Sets the Table Definition which will be used to determine the indexes to be used, 
     * passed though to the Criterions and validation of AttributeNames.
     * 
     * @param TableDefinition $oTableDefinition
     */
    public function setTableDefinition(TableDefinition $oTableDefinition): void
    {
        $this->oTableDefinition = $oTableDefinition;
    }

    /**
     * Process an array which contains user-defined criteria. This array was formatted in
     * the 'addCriteria'-method
     * 
     * @param  boolean $bIsNested Defines if this Criteria-object is nested within another
     *                            Criteria-object. This parameter should not be used 
     *                            externally. When a Criteria-object is nested, it will
     *                            not use any indexes.
     * 
     * @param  integer $iSubstituteIdentifier DynamoDB offers the ability to 'substitute' 
     *                                        used Attribute Names and Values with 
     *                                        placeholders. This is to prevent conflicts
     *                                        with reserved keywords like 'state'. This 
     *                                        class creates Criterion objects within an 
     *                                        array which - even nested - have their 
     *                                        unique SubsituteIdentifier, which results 
     *                                        in substitute names and values like #key0
     *                                        and :value0.
     * 
     * @return  integer Returns a SubsituteIdentifier. Because this method calls the same
     *                  method on nested Criteria-objects, it has to pass this integer
     *                  on to make sure that also nested AttributeNames and Values have 
     *                  an unique substitueidentifier.
     * 
     */
    public function processFormattedArrayOfCriteria(bool $bIsNested = false, int $iSubstituteIdentifier = 0): int
    {      
        if ($this->oTableDefinition === null) {
            throw new QueryException('Table definition is not available in Criteria object');
        }

        $aStructuredCriteria = $this->_preProcessFormattedCriteria($this->aFormattedCriteria);
        $aProcessedCriteria = [];

        foreach ($aStructuredCriteria as $sAttributeName => $aOperatorAndValues) {
            foreach ($aOperatorAndValues as $sOperator => $aValues) {
                if ($sOperator == 'between'
                    && is_array($aValues)
                    && isset($aValues[0])
                    && isset($aValues[1])) {
                    $oCriterion = new Criterion($iSubstituteIdentifier,
                                                $this->oTableDefinition,
                                                $sAttributeName,
                                                $sOperator,
                                                $aValues[0],
                                                $aValues[1]);
                    $aProcessedCriteria[$iSubstituteIdentifier++] = $oCriterion;
                } else {
                    foreach ($aValues as $mValue) {

                        $oCriterion = new Criterion($iSubstituteIdentifier,
                                                    $this->oTableDefinition,
                                                    $sAttributeName,
                                                    $this->aOperatorAliases[$sOperator] ?? $sOperator,
                                                    $mValue);
                        $aProcessedCriteria[$iSubstituteIdentifier++] = $oCriterion;
                    }
                }
            }
        }

        foreach ($this->aNestedCriteria as $oCriteria) {
            $oCriteria->setTableDefinition($this->oTableDefinition);
            $iSubstituteIdentifier = $oCriteria->processFormattedArrayOfCriteria(true, $iSubstituteIdentifier);
        }

        list($this->aAttributeNames, $this->aAttributeValues) 
            = $this->_getAttributeNamesAndValues($aProcessedCriteria, $this->aNestedCriteria);

        if (!$bIsNested) {
            $this->oUsedIndex = $this->_getBestIndex($aProcessedCriteria);    
        }

        list($this->aKeyConditionItems, $this->aFilterExpressionItems)
            = $this->_getKeyConditionAndFilterExpressionItems($aProcessedCriteria, $this->oUsedIndex, $bIsNested);

        return $iSubstituteIdentifier;
    }

    /**
     * Pre-process a formatted array of criteria
     * 
     * The method merges criteria which use the <= and >= operator on the same attribute
     * name to a between operator. Also, it merges equal criteria (same attributename,
     * operator and value) to a single criterion.
     * 
     * @param array $aFormattedCriteria. Array of formatted criteria 
     * 
     * @return array $aStructuredCriteria. 
     */
    private function _preProcessFormattedCriteria(array $aFormattedCriteria): array 
    {
        $aStructuredCriteria = [];
        //First structure all criteria to this format: [attributename][operator] = value
        foreach ($aFormattedCriteria as $iIndex => $aCriterionItem) {
            list($sAttributeName, $sOperator, $sAttributeValue) = $aCriterionItem;

            if (get_class($this) == ORCriteria::class) {
                $aStructuredCriteria[$sAttributeName][$sOperator][] = $sAttributeValue;
                continue;
            }

            if ($sOperator == '>='
                && isset($aStructuredCriteria[$sAttributeName]['<='])) {
                $aStructuredCriteria[$sAttributeName]['between'][0] = $sAttributeValue;
                $aStructuredCriteria[$sAttributeName]['between'][1] = reset($aStructuredCriteria[$sAttributeName]['<=']);
                unset($aStructuredCriteria[$sAttributeName]['<=']);
            } else if ($sOperator == '<='
                && isset($aStructuredCriteria[$sAttributeName]['>='])) {
                $aStructuredCriteria[$sAttributeName]['between'][0] = reset($aStructuredCriteria[$sAttributeName]['>=']);
                $aStructuredCriteria[$sAttributeName]['between'][1] = $sAttributeValue;
                unset($aStructuredCriteria[$sAttributeName]['>=']);
            } else {
                $aStructuredCriteria[$sAttributeName][$sOperator][] = $sAttributeValue;
                $aStructuredCriteria[$sAttributeName][$sOperator] = array_unique($aStructuredCriteria[$sAttributeName][$sOperator]);
            } 
        }
        return $aStructuredCriteria;
    }

    /**
     * Get attribute values and names for all criterions and nested criteria
     * 
     * @param  array $aProcessedCriteria An array of processed criteria as it is created in
     *                                   method 'processFormattedArrayOfCriteria'
     * 
     * @param  array $aNestedCriteria An array of nested criteria.
     * 
     * @return  array An array where the [0] contains all AttributeNames and [1] contains
     *                all AttributeValues
     */
    private function _getAttributeNamesAndValues(array $aProcessedCriteria, array $aNestedCriteria): array
    {
        $aMergedAttributeNames = [];
        $aMergedAttributeValues = [];

        //Merge attribute names and values of criterion into this object
        foreach ($aProcessedCriteria as $oCriterion) {
            $aAttributeNames = $oCriterion->getExpressionAttributeName();
            $aAttributeValues = $oCriterion->getExpressionAttributeValue();

            $aMergedAttributeNames = array_merge($aMergedAttributeNames, $aAttributeNames);
            $aMergedAttributeValues = array_merge($aMergedAttributeValues, $aAttributeValues);
        }

        //Merge attribute names and values of nested criteria into this object
        foreach ($aNestedCriteria as $oCriteria) {
            $aAttributeNames = $oCriteria->getExpressionAttributeNames();
            $aAttributeValues = $oCriteria->getExpressionAttributeValues();

            $aMergedAttributeNames = array_merge($aMergedAttributeNames, $aAttributeNames);
            $aMergedAttributeValues = array_merge($aMergedAttributeValues, $aAttributeValues);
        }

        return [
            $aMergedAttributeNames,
            $aMergedAttributeValues,
        ];
    }

    /**
     * The method returns all KeyConditions and FilterExpressions
     * 
     * @param  array $aProcessedCriteria An array of processed criteria as it is created in
     *                                   method 'processFormattedArrayOfCriteria'
     * 
     * @param  IndexDefinition $oUsedIndex An object which specifies the index used for this query/scan. 
     *                           This is the result of the '_getBestIndex'-method.
     * 
     * @param  boolean $bIsNested Whenever a Criteria-class is nested, all criterions (and
     *                            Criteria) will end up passing data to the FilterExpression
     *                            items and not the KeyCondition items.
     * 
     * @return  array [0] is in array of KeyCondition items and [1] is an array of 
     *                    FilterExpression items 
     */
    private function _getKeyConditionAndFilterExpressionItems(array $aProcessedCriteria, 
                                                            ?IndexDefinition $oUsedIndex, 
                                                            bool $bIsNested): array
    {       
        $aKeyConditionItems = [];
        $aFilterExpressionItems = [];
        foreach ($aProcessedCriteria as $iIndex => $oCriterion) {
            //Nested criteria never contribute to keyconditions and will always end up in filterexpressions
            if (!$bIsNested 
                && !empty($oUsedIndex)
                && ($oCriterion->getAttributeName() == $oUsedIndex->getHashKey()
                    || $oCriterion->getAttributeName() == $oUsedIndex->getSortKey())) {
                $sAttributeName = $oCriterion->getAttributeName();

                if (isset($aKeyConditionItems[$sAttributeName])) {
                    throw new InvalidQueryFormatException("AttributeName '$sAttributeName' can only be used once as a keycondition: " . print_r($this->aFormattedCriteria, true));
                }
                $aKeyConditionItems[$sAttributeName] = $oCriterion;
            } else {
                $aFilterExpressionItems[] = $oCriterion;
            }
        }
        return [
            $aKeyConditionItems,
            $aFilterExpressionItems,
        ];
    }

    /**
     * Finds the index to be used for this request based on the available indexes as
     * specified in the TableDefinition object, whether the operators used in the criteria
     * allow indexes to be used. At most one index can be (of which one is the primary key
     * 'index'). An index where both the primary hash and sort key are used is preferred over
     * one where only the hash key is used.
     * 
     * @param  array $aProcessedCriteria An array of processed criteria as it is created in
     *                                   method 'processFormattedArrayOfCriteria'
     * 
     * @return  IndexDefinition|null Returns the index to be used 
     */
    private function _getBestIndex(array $aProcessedCriteria): ?IndexDefinition
    {
        if (!$this->bEnableIndexes) {
            return null;
        }
        
        $aIndexDefinitions = $this->oTableDefinition->getIndexDefinitions();

        $aPossibleIndexesWithSortKey = [];
        $aPossibleIndexesWithoutSortKey = [];
        foreach ($aIndexDefinitions as $oIndex) {
            $bGotSuitableHashKey = false;
            $bUsesSortKey = false;
            //When one criterion in the full query contains a unsuitable operator which involves 
            //either the hash or sort key the index we're analyzing now, this index cannot be
            //used.
            $bIndexIsSuitable = true;
            foreach ($aProcessedCriteria as $oCriterion) {
                if ($oCriterion->getAttributeName() == $oIndex->getHashKey()
                    && $oCriterion->hasValidHashKeyConditionOperator()) {
                    $bGotSuitableHashKey = true;
                } 
                if ($oCriterion->getAttributeName() == $oIndex->getSortKey()
                    && $oCriterion->hasValidSortKeyConditionOperator()) {
                    $bUsesSortKey = true;
                } 

                //When an index is used, both hash and sort key need to be compatible
                //with the operators criteria use.
                if ($oCriterion->getAttributeName() == $oIndex->getHashKey()
                    && !($oCriterion->hasValidHashKeyConditionOperator())) {
                    $bIndexIsSuitable = false;
                } 
                if ($oCriterion->getAttributeName() == $oIndex->getSortKey()
                    && !($oCriterion->hasValidSortKeyConditionOperator())) {
                    $bIndexIsSuitable = false;
                } 
                //This code prevents use of indexes which both have a hash and sort-key
                //to be used, even if the criteria would only use the hash-key.
                // if (!$bUsesSortKey
                //     && !is_null($oIndex->getSortKey())) {
                //     $bIndexIsSuitable = false;
                // }
            }
            $iPriority = $oIndex->getPriority();
            if ($bIndexIsSuitable 
                && $bGotSuitableHashKey) {
                if ($bUsesSortKey) {
                    $aPossibleIndexesWithSortKey[$iPriority] = $oIndex;    
                } else {
                    $aPossibleIndexesWithoutSortKey[$iPriority] = $oIndex;
                }
            }
        }

        //The indexes are sorted using their index-value, what means that a lower value means
        //a higher priority over a higher index-value.
        if (count($aPossibleIndexesWithSortKey)) {
            ksort($aPossibleIndexesWithSortKey);    
            $oUsedIndex = reset($aPossibleIndexesWithSortKey);
        } else {
            ksort($aPossibleIndexesWithoutSortKey);    
            $oUsedIndex =  reset($aPossibleIndexesWithoutSortKey);
        }

        if (is_object($oUsedIndex)) {
            return $oUsedIndex;
        } else {
            return null;
        }
    }

    /**
     * Returns the used Index if used, otherwise NULL
     * 
     * @return  IndexDefinition|null
     */
    public function getUsedIndex(): ?IndexDefinition 
    {
        return $this->oUsedIndex;
    }

    /** 
     * Returns the FilterExpression based on the criteria 
     * 
     * @return  string The FilterExpressions as required by DynamoDB
     */
    public function getFilterExpression(): string
    {
        $aItems = [];
        foreach ($this->aFilterExpressionItems as $oCriterion) {
            $aItems[] = $oCriterion->getExpression();    
        }
        foreach ($this->aNestedCriteria as $oCriteria) {
            $aItems[] = sprintf("(%s)", $oCriteria->getFilterExpression());
        }
        return implode($this->getLogicalOperator(), $aItems);
    }

    /** 
     * Returns the KeyCondition based on the criteria and the chosen index. This value
     * will be an empty string when this object is a nested Criteria
     * 
     * @return  string The KeyCondition as required by DynamoDB. This will be for example:
     *                 #key0 = :value0 AND #key1 = :value1
     */
    public function getKeyConditionExpression(): string
    {
        $aItems = [];
        foreach ($this->aKeyConditionItems as $oCriterion) {
            $aItems[] = $oCriterion->getExpression();
        }
        //Key conditions are always using AND as logical operator
        return implode(' AND ', $aItems);
    }

    /**
     * Returns the mapping of the substitutes to the ExpressionAttribute values, as required
     * by DynamoDB. 
     * 
     * @return  array An array with the following structure:
     *                [
     *                    ':value1A' => [
     *                        'S' => 'Actual value'
     *                    ], 
     *                    ...
     *                ]
     */
    public function getExpressionAttributeValues(): array
    {
        return $this->aAttributeValues;
    }

    /**
     * Returns the mapping of the substitutes to the ExpressionAttributeNames, as required 
     * by DynamoDB.
     * 
     * @return  array An array with the following structure:
     *                [
     *                    '#key0' => 'Actual key, e.g. id',
     *                    ...
     *                ]
     */
    public function getExpressionAttributeNames(): array
    {
        return $this->aAttributeNames;
    }

    /**
     * Sets all operator aliases in one go
     * 
     * @param  array $aOperatorAliases
     */
    public function setOperatorAliases(array $aOperatorAliases): void 
    {
        $this->aOperatorAliases = $aOperatorAliases;
    }

    public function enableIndexes(bool $bEnableIndexes): void
    {
        $this->bEnableIndexes = $bEnableIndexes;
    }


}

