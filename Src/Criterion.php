<?php
namespace EoneoPay\DatabaseRepos;
use EoneoPay\DatabaseRepos\Exceptions\{UnsupportedOperatorException,UnsupportedTypeException,AttributeValueTypeMismatchException};

class Criterion
{
    /**
     * The following operators are valid in DynamoDB.
     */
    const VALID_OPERATORS = [
        'between',
        'begins_with',
        'contains',
        'attribute_exists',
        'attribute_not_exists',
        '<=',
        '>=',
        '=',
        '<',
        '>',
        '<>',
    ];

    /**
     * The following operators can be used in the KeyCondition string to hit an index.
     */
    const VALID_KEY_CONDITION_OPERATORS = [
        'between',
        'begins_with',
        '<=',
        '>=',
        '=',
        '<',
        '>',
    ];

    /**
     * The following operators need only an AttributeName and not an AttributeValue
     */
    const OPERATORS_ONLY_NAME_NEEDED = [
        'attribute_exists',
        'attribute_not_exists',
    ];

    private $sAttributeName;
    private $sAttributeOperator;
    private $sAttributeValue;
    private $oTableDefinition;

    //Every actual AttributeName and AttributeValue is substituted for a placeholder like
    //#key1 and :value1.  
    private $iSubstitutionId;

    //The second attribute value is used only for the BETWEEN operator
    private $sSecondAttributeValue;

    public function __construct(int $iSubstitutionId, 
                                TableDefinition $oTableDefinition, 
                                string $sAttributeName, 
                                string $sAttributeOperator, 
                                $sAttributeValue = null, 
                                $sSecondAttributeValue = null)
    {
        $this->sAttributeName = $sAttributeName;
        $this->sAttributeOperator = strtolower($sAttributeOperator);
        $this->sAttributeValue = $sAttributeValue;
        $this->oTableDefinition = $oTableDefinition;
        $this->iSubstitutionId = $iSubstitutionId;
        $this->sSecondAttributeValue = $sSecondAttributeValue;
        $this->_validateCriterion();
    }

    private function _validateCriterion(): void
    {
        if (!in_array($this->sAttributeOperator, self::VALID_OPERATORS)) {
            throw new UnsupportedOperatorException("Operator used on $this->sAttributeName is not valid");
        }

        if (in_array($this->sAttributeOperator, self::OPERATORS_ONLY_NAME_NEEDED)) {
            return;
        }

        if (!is_scalar($this->sAttributeValue) 
            && !is_null($this->sAttributeValue)) {
            throw new UnsupportedTypeException("Filtering on $this->sAttributeName on data types other than scalar and null is currently not supported");
        }

        if ($this->sAttributeOperator == 'between' 
            && (gettype($this->sAttributeValue) != gettype($this->sSecondAttributeValue))) {
            throw new AttributeValueTypeMismatchException("Between used on $this->sAttributeName uses different date types");
        }
    }

    /**
     * Checks whether this Criterion uses an operator which would work together with a sort-key. If this method
     * returns false, there is a smaller chance the index will be used.
     * 
     * @return  boolean See description
     */
    public function hasValidSortKeyConditionOperator(): bool
    {
        return in_array($this->sAttributeOperator, self::VALID_KEY_CONDITION_OPERATORS);
    }

    /**
     * Checks whether this Criterion uses an operator which would work together with a hash-key. If this
     * returns false, the index will not be used.
     * 
     * @return  boolean See description
     */
    public function hasValidHashKeyConditionOperator(): bool
    {
        return $this->sAttributeOperator == '=';
    }

    /**
     * Returns the AttributeName the operator operates on.
     * 
     * @return  string AttributeName
     */
    public function getAttributeName(): string
    {
        return $this->sAttributeName;
    }

    /**
     * 
     * 
     */
    public function getExpressionAttributeValue(): array
    {
        if (in_array($this->sAttributeOperator, self::OPERATORS_ONLY_NAME_NEEDED)) {
            return [];
        }

        $aExpressionAttributeValues = [];
        $i = 0;
        foreach ([$this->sAttributeValue, $this->sSecondAttributeValue] as $sAttributeValue) {
            if ($i === 1 && is_null($sAttributeValue)) {
                //Here is checked if we arrived at the second attribute. If that's null, skip it
                //because this if's purpose is to support BETWEEN operator which does not
                //support a NULL operand anyway 
                continue;
            }
            $sSubstituteValue = sprintf(":value%d%s", $this->iSubstitutionId, range('A', 'Z')[$i++]);

            $aDynamoFieldTypeAndTransformedValue = $this->oTableDefinition->getDynamoFieldTypeAndTransformValue($this->sAttributeName, $sAttributeValue, true);
            $sDynamoFieldType = $aDynamoFieldTypeAndTransformedValue['DynamoType'];
            $sTransformedValue = $aDynamoFieldTypeAndTransformedValue['TransformedValue'];

            $aExpressionAttributeValues = array_merge([
                $sSubstituteValue => [
                    $sDynamoFieldType => $sTransformedValue
                ]
            ], $aExpressionAttributeValues);
        }
        return $aExpressionAttributeValues;
    }

    /**
     * Returns the array member which maps the actual AttributeName to the substitute based on the 
     * unique Substitute.
     * 
     * @return  array See description
     */
    public function getExpressionAttributeName(): array
    {
        $sSubstituteName = sprintf("#key%d", $this->iSubstitutionId);
        return [
            $sSubstituteName => $this->sAttributeName
        ];
    }

    /**
     * Returns the part of the FilterExpression or the KeyCondition for this Criterion, based on the 
     * unique Substitution ID given, the Attribute Name, Value(s) and the operator.
     * 
     * @return  string See description
     */
    public function getExpression(): string
    {
        switch($this->sAttributeOperator) {
            case 'attribute_not_exists':
            case 'attribute_exists':
                return sprintf('%s (#key%d)', $this->sAttributeOperator, $this->iSubstitutionId);
            case 'between':
                return sprintf('#key%d %s :value%dA AND :value%dB', $this->iSubstitutionId, $this->sAttributeOperator, $this->iSubstitutionId, $this->iSubstitutionId);
            case 'contains':
            case 'begins_with':
                return sprintf('%s (#key%d,:value%dA)', $this->sAttributeOperator, $this->iSubstitutionId, $this->iSubstitutionId);
            default:
                return sprintf('#key%d %s :value%dA', $this->iSubstitutionId, $this->sAttributeOperator, $this->iSubstitutionId);        
        }
    }
}
