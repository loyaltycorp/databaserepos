<?php
namespace EoneoPay\DatabaseRepos;

class TransactionObjectRepository extends BaseRepository
{
    protected $oTableDefinition;

    public function __construct(Connection $oConnection = null, string $sTableName)
    {
        $this->oTableDefinition = new TableDefinition($sTableName, [

        ],
        [
            'TransactionID'         => 'string',
            'TableNameKeySchema'    => 'string',
            'ObjectState'           => 'object',
        ], 'TransactionID', 'TableNameKeySchema');

        parent::__construct($oConnection, 
                            Transaction::class, 
                            $this->oTableDefinition);
    }

    public function getTableDefinition()
    {
        return $this->oTableDefinition;
    }
}
