<?php
namespace EoneoPay\DatabaseRepos;
use ArrayAccess;
use Aws\DynamoDb\Exception\DynamoDbException;
use EoneoPay\DatabaseRepos\Exceptions\{DatabaseReposException,SaveObjectException,
    DeleteObjectException,QueryException,ConditionalCheckFailedException};

abstract class BaseRepository
{
    private $aDebugInfo = [];
    protected $oTableDefinition;
    protected $sClassName;
    private $oConnection;

    private $bEnableIndexes = true;
    private $aOperatorAliases = [];

    private $oCreateCollectionClosure = null;
    private $oCreatePaginatorClosure = null;
    private $oPrepareToSaveClosure = null;
    private $oAfterObjectResolvementClosure = null;
    private $oLoggingClosure = null;

    const LOG_DEBUG = 0;
    const LOG_NOTICE = 1;
    const LOG_WARNING = 2;
    const LOG_ERR = 3;

    const ACTION_SAVE = 0;
    const ACTION_DELETE = 1;
    const ACTION_QUERY = 2;

    /**
     * Creates a Repository which performs actions on a table as specified
     * in the TableDefinition using the Connection object. Different actions
     * e.g. saving, deleting and querying this table results in calling the
     * different Closures. These closures can be used to integrate this package
     * into an existing environment.
     * 
     * @param  Connection $oConnection The Connection object which will be used to 
     * 
     * @param  string $sClassName The classname of the objects which will be saved or 
     *                            outputted
     * @param  TableDefinition $oTableDefinition The TableDefinition defines the schema
     *                                           and the indexes used in a table. It is
     *                                           important to know what indexes exist
     *                                           so that either a Query of Scan action can 
     *                                           be sued to get the data.
     * 
     * @param  \Closure $oCreateCollectionClosure See BaseRepository::setCreateCollectionClosure()
     * 
     * @param  \Closure $oCreatePaginatorClosure Not implemented yet
     * 
     * @param  \Closure $oPrepareToSaveClosure See BaseRepository::setPrepareToSaveClosure()
     */
    public function __construct(Connection $oConnection = null, 
                                string $sClassName, 
                                TableDefinition $oTableDefinition, 
                                \Closure $oCreateCollectionClosure = null,
                                \Closure $oCreatePaginatorClosure = null,
                                \Closure $oPrepareToSaveClosure = null)
    {
        $this->oTableDefinition = $oTableDefinition;
        $this->sClassName = $sClassName;
        $this->oConnection = $oConnection;
        $this->oCreateCollectionClosure = $oCreateCollectionClosure;
        $this->oCreatePaginatorClosure = $oCreatePaginatorClosure;
        $this->oPrepareToSaveClosure = $oPrepareToSaveClosure;
    }

    /**
     * Sets the Connection object used in future requests
     * 
     * @param  Connection $oConnection 
     */
    public function setConnection(Connection $oConnection): void
    {
        $this->oConnection = $oConnection;
    }

    /**
     * Sets a closure which is called when creating collection. The closure must accept
     * an array of objects as the first parameter. If NULL is given, collections
     * will be returned as arrays of objects.
     * 
     * @param  \Closure $oCreateCollectionClosure 
     */
    public function setCreateCollectionClosure(?\Closure $oCreateCollectionClosure = null): void
    {
        $this->oCreateCollectionClosure = $oCreateCollectionClosure;
    }


    public function setCreatePaginatorClosure(?\Closure $oCreatePaginatorClosure = null): void
    {
        $this->oCreatePaginatorClosure = $oCreatePaginatorClosure;
    }

    /**
     * Sets a closure which is called every time an object is saved in this class' 
     * saveObject method. The closure must accept an object as parameter (the to be
     * saved object) and must return an array of key-value pairs which represent the
     * object in an array.
     * 
     * @param  \Closure $oPrepareToSaveClosure 
     */
    public function setPrepareToSaveClosure(?\Closure $oPrepareToSaveClosure = null): void
    {
        $this->oPrepareToSaveClosure = $oPrepareToSaveClosure;
    }

    /**
     * Sets a closure which is run every time after the object has been resolved. The closure
     * should accept one argument which is an instance of the class as specified by 
     * $this->sClassName. 
     * 
     * @param  \Closure $oAfterObjectResolvementClosure 
     */
    public function setAfterObjectResolvementClosure(?\Closure $oAfterObjectResolvementClosure = null): void
    {
        $this->oAfterObjectResolvementClosure = $oAfterObjectResolvementClosure;
    }

    /**
     * Sets a closure which is called whenever this package logs. The closure will receive
     * data which can be output within the closure to a logging package or standard output.
     * 
     * @param  \Closure $oLoggingClosure See below:
     * 
     * The closure receives the following parameters:
     * function ($iLogLevel, $sAction, $sTableName, $sClassName, $sLogLine)
     * 
     * $iLogLevel   Is the value of one of the LOG_ constants defined in this class
     * $sAction     Is the value of one of the ACTION_constants defined in this class
     * $sTableName  Is the value of the table this repository works on
     * $sClassName  Is the value of the classname this repository accepts and outputs
     * $sLogLine    Contains the actual logline(s)
     */
    public function setLoggingClosure(?\Closure $oLoggingClosure): void 
    {
        $this->oLoggingClosure = $oLoggingClosure;
    }

    /**
     * Returns the TableDefinition set on this Repository
     * 
     * @return  TableDefinition 
     */
    public function getTableDefinition(): TableDefinition
    {
        return $this->oTableDefinition;
    }

    /**
     * Deletes an object from the Database
     * 
     * @param  object $oObject The object of a class as specified when constructing this
     *                         Repository. 
     */
    public function deleteObject($oObject)
    {
        if (get_class($oObject) !== $this->sClassName) {
            throw new DeleteObjectException('Given object is not an instance of ' . $this->sClassName);
        }

        $aObject = $this->_prepareObjectForSaveOrDelete($oObject);

        if (!isset($aObject[$this->oTableDefinition->getPrimaryHashKey()])) {
            throw new DeleteObjectException("Object does not have primary hash-key with name '{$this->oTableDefinition->getPrimaryHashKey()}'");
        }

        $aPrimaryKeys[$this->oTableDefinition->getPrimaryHashKey()] = $aObject[$this->oTableDefinition->getPrimaryHashKey()];

        if ($this->oTableDefinition->getPrimarySortKey() !== null 
            && !isset($aObject[$this->oTableDefinition->getPrimarySortKey()])) {
            throw new DeleteObjectException("Object does not have a primary sort-key with name '{$this->oTableDefinition->getPrimarySortKey()}'");
        } elseif ($this->oTableDefinition->getPrimarySortKey() !== null) {
            $aPrimaryKeys[$this->oTableDefinition->getPrimarySortKey()] = $aObject[$this->oTableDefinition->getPrimarySortKey()];
        }

        $aParams = [
            'TableName' => $this->oTableDefinition->getTableName(),
            'Key'       => $this->_processArrayToQuery($aPrimaryKeys)
        ];
        $this->_log(self::LOG_DEBUG, self::ACTION_DELETE, '$aParams: ' .  print_r($aParams, true));

        try {
            $oResponse = $this->oConnection->getClient()->deleteItem($aParams);    
        } catch (DynamoDbException $oException) {
            throw new DeleteObjectException("Can not delete object: " . $oException->getMessage());
        }
    }

    /**
     * Saves an object to the Database
     * 
     * @param  object $oObject The object of a class as specified when constructing this
     *                         Repository. The object must have the primary key(s) set.
     *                         That is, when a table uses a composite (hash and sort-key)
     *                         the object must have both, otherwise only the hash-key.
     * 
     * @param  boolean $bDoCommit Specifies whether the object should be saved. If true, the
     *                            object will be saved. If false, the params required to save
     *                            the object will be returned. Default is true.
     * 
     * @param  array $aConditionsForWrite The conditions which must evaluate to TRUE. If any
     *                                    conditions evaluates to false, a SaveObjectException
     *                                    will be thrown.
     * 
     * @return  null|array Returns null when $bDoCommit is true. Returns the DynamoDB-params
     *                     when $bDoCommit is false
     */
    public function saveObject($oObject, 
                                bool $bDoCommit = true,
                                array $aConditionsForWrite = []
        ): ?array
    {
        $this->_log(self::LOG_DEBUG, self::ACTION_SAVE, '$oObject: ' .  print_r($oObject, true));

        if (!is_object($oObject)) {
            throw new SaveObjectException('Given variable is not an object');
        }

        if (get_class($oObject) !== $this->sClassName) {
            throw new SaveObjectException('Given object is not an instance of ' . $this->sClassName);
        }

        if (!$bDoCommit 
            && count($aConditionsForWrite)) {
            throw new SaveObjectException("Can not add conditions on params which will be later used for batch writing.");
        }

        $aObject = $this->_prepareObjectForSaveOrDelete($oObject);

        if (!isset($aObject[$this->oTableDefinition->getPrimaryHashKey()])) {
            throw new SaveObjectException("Object does not define primary hash-key for AttributeName '{$this->oTableDefinition->getPrimaryHashKey()}'");
        }

        if ($this->oTableDefinition->getPrimarySortKey() !== null 
            && !isset($aObject[$this->oTableDefinition->getPrimarySortKey()])) {
            throw new SaveObjectException("Object does not define primary sort-key with AttributeName '{$this->oTableDefinition->getPrimarySortKey()}'");
        }


        $aItem = $this->_processArrayToQuery($aObject);

        $aParams = [
            'TableName' => $this->oTableDefinition->getTableName(),
            'Item'      => $aItem,
        ];

        if (count($aConditionsForWrite)) {
            $oCriteria = new ANDCriteria($aConditionsForWrite);
            $oCriteria->setTableDefinition($this->oTableDefinition);
            $oCriteria->enableIndexes(false);
            $oCriteria->setOperatorAliases($this->aOperatorAliases);
            $oCriteria->processFormattedArrayOfCriteria();
            $aParams = array_merge([
                'ConditionExpression'       => $oCriteria->getFilterExpression(),
                'ExpressionAttributeNames'  => $oCriteria->getExpressionAttributeNames(),
                'ExpressionAttributeValues' => $oCriteria->getExpressionAttributeValues(),
            ], $aParams);
        }

        if (!$bDoCommit) {
            return ['Item' => $aItem];
        }
        $aParams = $this->_unsetEmptyArrayMembers($aParams);
        

        $this->_log(self::LOG_DEBUG, self::ACTION_SAVE, '$aParams: ' . print_r($aParams, true));

        try {
            $oResponse = $this->oConnection->getClient()->putItem($aParams);
        } catch (DynamoDbException $oException) {
            switch ($oException->getAwsErrorCode()) {
                case 'ConditionalCheckFailedException':
                    throw new ConditionalCheckFailedException("Could not save object, condition failed");
                default:
                    throw new SaveObjectException("Can not save object: " . $oException->getMessage());
            }
        }
        return null;
    }

    public function saveMultipleObjects(array $aObjects) 
    {
        foreach ($aObjects as $oObject) {
            $aPutRequests[] = ['PutRequest' => $this->saveObject($oObject, false)];
        } 

        $aParams = [
            'RequestItems' => [
                $this->oTableDefinition->getTableName() => $aPutRequests,
            ],
        ];

        try {
            $oResponse = $this->oConnection->getClient()->batchWriteItem($aParams);
        } catch (DynamoDbException $oException) {
            echo json_encode($oException);
            throw new SaveObjectException("Can not save object: " . $oException->getMessage());
        }
    }

    /**
     * Gets an object by the primary hash-id. This method is not done yet.
     * 
     * @param  string $sId The primary hash ID. At this point this method does not support 
     *                     composite keys yet.
     * 
     * @return object      The object of a class as specified when constructing this
     *                     Repository. 
     */
    public function getObjectById(string $sId)
    {
        //TODO: Update this one, and try-catch
        $aResponse = $this->oConnection->getClient()->getItem([
            'TableName' => $this->oTableDefinition->getTableName(),
            'Key' => [
                'id' => [ 'N' => $sId]
            ]
        ]);

        $oObject = $this->_createObjectFromItem($aResponse['Item']);
        $this->setDebugInfo('LAST_OPERATION_TYPE', 'GETITEM');
        return $oObject;
    }

    /**
     * Gets a collection/array of objects according to the specified Criteria. The different
     * array members are 'combined' using an AND operator.
     * 
     * @param  array $aCriteria The criteria for the request. See the PHPDOC at 
     *                          Criteria::addCriteria for an explanation of the format
     *                          of this array.
     * 
     * @param  boolean $bReturnBackwardsSorted Specifies whether the returned collection/array
     *                                         should be sorted backwards or forwards.
     * 
     * @param  integer $iLimit Specifies the amount of items to return in the collection
     * 
     * @param  integer $iOffset Specifies at what offset the first item should start. Mind that
     *                          all the items before this offset are 'skipped' but are part of 
     *                          the response. This parameter is used when $aNextVector is used.
     * 
     * @param  array $aNextVector The next vector is a key-value pair representing the primary
     *                            key value(s) of the last item of the last request. This parameter is 
     *                            comparable to using an offset. 
     * 
     * @return  array/Collection An array or other collection object (if transformed using the
     *                           $oCreateCollectionClosure) containing all objects of the 
     *                           classname as specified the $sClassName property
     */
    public function getObjectsByANDCriteria(array $aCriteria = [], 
                                            bool $bReturnBackwardsSorted = false,
                                            int $iLimit = null, 
                                            int $iOffset = null,
                                            array $aNextVector = null)
    {

        $this->_log(self::LOG_DEBUG, self::ACTION_QUERY, sprintf('$iLimit: %d $iOffset: %d $aCriteria: %s', $iLimit, $iOffset, print_r($aCriteria, true)));
        $oCriteria = new ANDCriteria($aCriteria);
        return $this->getObjectsByCriteria($oCriteria, $bReturnBackwardsSorted, $iLimit, $iOffset);
    }

    /**
     * This method requires the same parameters as BaseRepository::getObjectsByANDCriteria but requires a
     * Criteria object rather than an array. See that method for a description.
     */
    public function getObjectsByCriteria(Criteria $oCriteria, 
                                        bool $bReturnBackwardsSorted = false,
                                        int $iLimit = null,
                                        int $iOffset = null,
                                        array $aNextVector = null)
    {
        $oCriteria->setTableDefinition($this->oTableDefinition);
        $oCriteria->enableIndexes($this->bEnableIndexes);
        $oCriteria->setOperatorAliases($this->aOperatorAliases);

        try {
            $oCriteria->processFormattedArrayOfCriteria();
        } catch (DatabaseReposException $oException) {
            $this->_log(self::LOG_WARNING, self::ACTION_QUERY, 'No results returned because of a fault in the query: ' . $oException->getMessage());
            return $this->_returnCollectionOrArray([]);
        } 
        
        $aParams = [
            'TableName'                 => $this->oTableDefinition->getTableName(),
            'IndexName'                 => ($oCriteria->getUsedIndex() 
                                            && !$oCriteria->getUsedIndex()->isPrimaryKey()) 
                                                ? $oCriteria->getUsedIndex()->getIndexName() 
                                                : null,
            'FilterExpression'          => $oCriteria->getFilterExpression(),
            'KeyConditionExpression'    => $oCriteria->getKeyConditionExpression(),
            'ExpressionAttributeNames'  => $oCriteria->getExpressionAttributeNames(),
            'ExpressionAttributeValues' => $oCriteria->getExpressionAttributeValues(),
        ];
        if ($iLimit !== null && empty($iOffset)) {
            $aParams['Limit'] = $iLimit;
        }

        $sExclusiveStartKey = $this->_processArrayToQuery($aNextVector ?? []);
        if (!empty($sExclusiveStartKey)) {
            $aParams['ExclusiveStartKey'] = $sExclusiveStartKey;
        }

        $aParams = $this->_unsetEmptyArrayMembers($aParams);

        if ($bReturnBackwardsSorted) {
            $aParams['ScanIndexForward'] = false;
        }
        $this->_log(self::LOG_DEBUG, self::ACTION_QUERY, '$aParams: ' . print_r($aParams, true));
        try {
            if ($oCriteria->getUsedIndex()) {
                $this->setDebugInfo('LAST_OPERATION_TYPE', 'QUERY');
                $oPaginator = $this->oConnection->getClient()->getPaginator('Query', $aParams); 
            } else {
                $this->setDebugInfo('LAST_OPERATION_TYPE', 'SCAN');
                $oPaginator = $this->oConnection->getClient()->getPaginator('Scan', $aParams);   
            }

            $this->setDebugInfo('LAST_USED_SORT_KEY', ($oCriteria->getUsedIndex()) 
                                                        ? $oCriteria->getUsedIndex()->getSortKey() 
                                                        : null);
            $this->setDebugInfo('LAST_USED_INDEX_NAME', ($oCriteria->getUsedIndex()) 
                                                        ? $oCriteria->getUsedIndex()->getIndexName() 
                                                        : null);

            $aObjects = [];
            $iAddedObjects = 0;

            foreach ($oPaginator as $iKey => $aResult) {
                $this->_log(self::LOG_DEBUG, self::ACTION_QUERY, sprintf('$oPaginator $iKey: %d $aResult: %s', $iKey, print_r($aResult, true)));
                if (isset($aResult['Items'])) {
                    $aItems = $aResult['Items'];
                } else {
                    break;
                }
                foreach ($aResult['Items'] as $iIndex => $aItem) {
                    
                    //Ignore offset if ExclusiveStartKey is used (which is an actual offset)
                    if (($iIndex >= $iOffset
                            || $iOffset === null
                            || !empty($sExclusiveStartKey))
                        && ($iAddedObjects < $iLimit
                            || $iLimit === null)) {
                        $oObject = $this->_createObjectFromItem($aItem);
                        $aObjects[] = $oObject;
                        $iAddedObjects++;
                    }
                }
                if ($iAddedObjects >= $iLimit) {
                    break;
                }
            }
        } catch (DynamoDbException $oException) {
            $sMsg = "An error occured while do an operation on table '{$this->getTableDefinition()->getTableName()}': " .  $oException->getMessage();
            $this->_log(self::LOG_ERR, self::ACTION_QUERY, $sMsg);
            throw new QueryException($sMsg);
        }
        $this->_log(self::LOG_DEBUG, self::ACTION_QUERY, '$aObjects: ' . print_r($aObjects, true));

        $aNextVector = [];
        if (isset($aResult['LastEvaluatedKey'])) {
            foreach ($aResult['LastEvaluatedKey'] as $sAttributeName => $sAttributeTypeAndValue) {
                $aNextVector[$sAttributeName] = reset($sAttributeTypeAndValue);
            }
        }

        $aPaginatorData = [
            'NextVector' => $aNextVector,
            'Limit'      => $iLimit,
            'Offset'     => $iOffset ?? 0,
        ];

        if ($this->oCreatePaginatorClosure !== null) {
            $this->oCreatePaginatorClosure->call($this, $aPaginatorData);
        }

        return $this->_returnCollectionOrArray($aObjects);
    }

    public function enableIndexes(bool $bEnabled): void
    {
        $this->bEnableIndexes = $bEnabled;
    }

    private function setDebugInfo(string $sKey, ?string $sValue)
    {
        $this->aDebugInfo[$sKey] = $sValue;
    }

    public function getDebugInfo(string $sKey): ?string
    {
        return $this->aDebugInfo[$sKey] ?? null;
    }

    /**
     * This method creates a collection from an array if the CreateCollectionClosure 
     * is set or an array if it is not.
     * 
     * @param  array $aObjects 
     * @return array|Object 
     */
    private function _returnCollectionOrArray(array $aObjects) 
    {
        if ($this->oCreateCollectionClosure !== null) {
            return $this->oCreateCollectionClosure->call($this, $aObjects);
        }
        return $aObjects;
    }

    /**
     * This method calls the oPrepareToSaveClosure to transform an object to an array
     * 
     * @param  object $oObject 
     * @return  array 
     */
    private function _prepareObjectForSaveOrDelete($oObject): array
    {
        if ($this->oPrepareToSaveClosure instanceof \Closure) {
            $oPrepareToSaveClosure = $this->oPrepareToSaveClosure;
            $aObject = $oPrepareToSaveClosure($oObject);
        } else {
            $aObject = get_object_vars($oObject);
        }
        return $aObject;
    }

    /**
     * Creates an object from an item as it is received from out of the Database. 
     * 
     * @param   $aItemArray Is an array with the DynamoDB datatypes specification. 
     * 
     * @param   $sElementType Specifies the element type.
     * 
     * @return object      The object of a class as specified when constructing this
     *                     Repository. 
     */
    private function _createObjectFromItem(array $aItemArray, string $sElementType = 'class')
    {
        switch ($sElementType) {
            case 'class':
                $mItem = new $this->sClassName;
                break;
            case 'object':
                $mItem = new \stdClass;
                break;
            case 'array':
                $mItem = [];
                break;
        }

        foreach ($aItemArray as $sPropertyKey => $mPropertyTypeAndValue) {
            $mPropertyValue = reset($mPropertyTypeAndValue);
            $mPropertyDynamoType = key($mPropertyTypeAndValue);

            switch ($mPropertyDynamoType) {
                case 'L':
                    $mNewMember = $this->_createObjectFromItem($mPropertyValue, 'array');
                    break;
                case 'M':
                    $mNewMember = $this->_createObjectFromItem($mPropertyValue, 'array');
                    break;
                case 'NULL':
                    $mNewMember = "";
                    break;
                case 'N':
                    if ((int) $mPropertyValue == $mPropertyValue) {
                        $mNewMember = intval($mPropertyValue);
                    } else {
                        $mNewMember = floatval($mPropertyValue);
                    }
                    break;
                case 'BOOL':
                    $mNewMember = boolval($mPropertyValue);
                    break;
                case 'S':
                    $mNewMember = strval($mPropertyValue);
                    break;
                default:
                    $mNewMember = $mPropertyValue;
                    break;
            }

            switch ($sElementType) {
                case 'object':
                case 'class':
                    $mItem->$sPropertyKey = $mNewMember;
                    break;
                case 'array':
                    $mItem[$sPropertyKey] = $mNewMember;
                    break;
            }
        }

        //This assumes that $sElementType == 'class' only when the root of an object
        //is being resolved.
        if ($sElementType == 'class'
            && $this->oAfterObjectResolvementClosure instanceof \Closure) {
            $this->oAfterObjectResolvementClosure->call($this, $mItem);
        }
        return $mItem;
    } 

    /**
     * Set operator aliases 
     * 
     * @param  array $aOperatorAliases 
     */
    public function setOperatorAliases(array $aOperatorAliases): void
    {
        $this->aOperatorAliases = $aOperatorAliases;
    }

    /**
     * Unset all empty array members
     * 
     * @param  array $aArray
     * 
     * @return array
     */
    private function _unsetEmptyArrayMembers(array $aArray): array
    {
        foreach ($aArray as $sKey => $mValue) {
            if (empty($mValue)) {
                unset($aArray[$sKey]);
            }
        }
        return $aArray;
    }
    /**
     * Processes an array of key-value pairs, part of an object to be saved or the 
     * KeySchema to delete the object for.
     * 
     * @param  mixed  $mItem 
     * 
     * @return  array Returns the Item but with the DynamoDB datatypes for each 
     *                member
     */
    private function _processArrayToQuery($mItem, bool $bUseSchema = true): array
    {
        $aItemQuery = [];
        foreach ($mItem as $sKey => $mValue) {
            if (is_array($mValue) || is_object($mValue)) {
                $mValue = $this->_processArrayToQuery($mValue, false);
            }
            
            $aDynamoFieldTypeAndTransformedValue = $this->oTableDefinition->getDynamoFieldTypeAndTransformValue($sKey, $mValue, $bUseSchema);
            $sDynamoFieldType = $aDynamoFieldTypeAndTransformedValue['DynamoType'];
            $mTransformedValue = $aDynamoFieldTypeAndTransformedValue['TransformedValue'];
            
            $aItemQuery[$sKey][$sDynamoFieldType] = $mTransformedValue;    
        }
        return $aItemQuery;
    }

    /**
     * This internal function is used in the Repository to log lines. Calls the logging
     * Closure
     * 
     * @param  int     $iLogLevel   Is the value of one of the LOG_ constants defined in this class
     * @param  string  $sAction     Is the value of one of the ACTION_constants defined in this class
     * @param  string  $sLogLine    Contains the actual logline(s)
     */
    private function _log(int $iLogLevel, string $sAction, string $sLogLine): void
    {
        if ($this->oLoggingClosure instanceof \Closure) {
            $this->oLoggingClosure->call($this, $iLogLevel, $sAction, $this->oTableDefinition->getTableName(), $this->sClassName, $sLogLine);    
        }
    }
}
