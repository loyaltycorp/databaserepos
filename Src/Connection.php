<?php
namespace EoneoPay\DatabaseRepos;
use Aws\Sdk;

class Connection
{
    private $oDynamoDb;

    public function __construct(string $sEndPoint,
                                string $sRegion,
                                ?string $sVersion,
                                ?string $sKey,
                                ?string $sSecret)
    {
        $aParams = [];
        $aParams = [
            'endpoint'  => $sEndPoint,  
            'region'    => $sRegion,  
        ];

        if (!empty($sVersion)) {
            $aParams['version'] = $sVersion;
        }

        if (!empty($sKey)
            && !empty($sSecret)) {
            $aParams['credentials'] = [
                'key'       => $sKey,
                'secret'    => $sSecret,
            ];
        }

        $oSdk = new Sdk($aParams);

        // Create a new DynamoDB client
        $this->oDynamoDb = $oSdk->createDynamoDb();
    }

    public function getClient()
    {
        return $this->oDynamoDb;
    }

}
