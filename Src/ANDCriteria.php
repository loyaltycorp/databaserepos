<?php
namespace EoneoPay\DatabaseRepos;
class ANDCriteria extends Criteria
{
    public function getLogicalOperator(): string
    {
        return ' AND ';
    }
}
