<?php
namespace EoneoPay\DatabaseRepos;

class Migration
{
    private $oOldTableDefinition;
    private $oNewTableDefinition;

    /**
     * Constructs a Migration object. 
     * 
     * @param TableDefinition $oOldTableDefinition The TableDefinition to migrate
     *                                             from.
     * 
     * @param TableDefinition $oNewTableDefinition The TableDefinition to migrate to
     *                                             with added/deleted indexes and/or
     *                                             other changes.
     */
    public function __construct(TableDefinition $oOldTableDefinition,
                                TableDefinition $oNewTableDefinition)
    {
        $this->oOldTableDefinition = $oOldTableDefinition;
        $this->oNewTableDefinition = $oNewTableDefinition;
    }

    /**
     * Migrate from the old table definition to the new table definition
     * 
     * @param  TableManager $oTableManager TableManager to use for the 
     *                                     execution
     * 
     * @return  bool Returns true is the migration has performed an action
     *               or false when nothings has happened.
     */
    public function execute(TableManager $oTableManager): bool
    {
        $bActionsExecuted = false;

        $aOldIndexDefinitions = $this->oOldTableDefinition->getIndexDefinitions();
        $aNewIndexDefinitions = $this->oNewTableDefinition->getIndexDefinitions();

        //Delete indexes which are not in the new tabledefinition and rebuild the ones 
        //which have changed.
        foreach ($aOldIndexDefinitions as $oOldIndexDefinition) {
            if ($oOldIndexDefinition->isPrimaryKey()) {
                //Skip the primary key
                continue;
            }
            $oNewIndexDefinition = $aNewIndexDefinitions[$oOldIndexDefinition->getIndexName()] ?? null;
            if (is_null($oNewIndexDefinition)) {
                //Delete index
                $oTableManager->deleteIndex($this->oOldTableDefinition, 
                                            $oOldIndexDefinition->getIndexName());
                $bActionsExecuted = true;
            } else if ($this->_areIndexDefinitionsSignificantlyDifferent($oOldIndexDefinition, 
                                                                        $oNewIndexDefinition)) {
                //Delete index and create new one again
                $oTableManager->deleteIndex($this->oOldTableDefinition, 
                                            $oOldIndexDefinition->getIndexName());
                
                $oTableManager->createIndexFromIndexDefinition($this->oNewTableDefinition,
                                                                $oNewIndexDefinition);
                $bActionsExecuted = true;
            }
        }

        //Create indexes which are not in the old index definitions
        foreach ($aNewIndexDefinitions as $oNewIndexDefinition) {
            $oOldIndexDefinition = $aOldIndexDefinitions[$oNewIndexDefinition->getIndexName()] ?? null;
            if (is_null($oOldIndexDefinition)) {
                $oTableManager->createIndexFromIndexDefinition($this->oNewTableDefinition,
                                                                $oNewIndexDefinition);
                $bActionsExecuted = true;
            }
        }
        return $bActionsExecuted;
    }

    private function _areIndexDefinitionsSignificantlyDifferent(IndexDefinition $oOldIndexDefinition,
                                                                IndexDefinition $oNewIndexDefinition) {
        return $oOldIndexDefinition->isSignificantlyDifferentThan($oNewIndexDefinition)
            || $this->oOldTableDefinition->getTypeForAttribute($oOldIndexDefinition->getHashKey())
                != $this->oNewTableDefinition->getTypeForAttribute($oNewIndexDefinition->getHashKey())
            || $this->oOldTableDefinition->getTypeForAttribute($oOldIndexDefinition->getSortKey())
                != $this->oNewTableDefinition->getTypeForAttribute($oNewIndexDefinition->getSortKey());
    }
}
