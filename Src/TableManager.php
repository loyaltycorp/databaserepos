<?php
namespace EoneoPay\DatabaseRepos;
use Aws\DynamoDb\Exception\DynamoDbException;
use EoneoPay\DatabaseRepos\Exceptions\TableManagerException;

class TableManager 
{
    protected $oConnection;

    //There mappings are only used for translating key type definitions
    const ATTRIBUTETYPESMAPPING = [
        'integer'   => 'N',
        'string'    => 'S',
    ];

    //This property is used to determine what datatype a certain field is, to keep every operation
    //consistent
    protected $aAttributeDefinitions = [];

    public function __construct(Connection $oConnection)
    {
        $this->oConnection = $oConnection;
    }

    public function deleteTable(string $sTableName): void
    {
        try {
            $oResponse = $this->oConnection->getClient()->deleteTable(['TableName' => $sTableName]);
            $this->oConnection->getClient()->waitUntil('TableNotExists', [
                'TableName' => $sTableName,
                '@waiter' => [
                    'delay'       => 5,
                    'maxAttempts' => 20
                ]
            ]); 
        } catch (DynamoDbException $e) {
            throw new TableManagerException("Can not delete table $sTableName: " . $e->getMessage());
        }
    } 

    public function doesTableExist(string $sTableName): bool
    {
        return in_array($sTableName, $this->getListOfTables());
    }   

    public function getListOfTables(): array
    {
        try {
            $oResponse = $this->oConnection->getClient()->listTables([]);
            return array_values($oResponse['TableNames']);
        } catch (DynamoDbException $e) {
            throw new TableManagerException("Can not list tables: " . $e->getMessage());
        }
    }

    public function getTableInfo(string $sTableName)
    {
        try {
            $oResponse = $this->oConnection->getClient()->describeTable(['TableName' => $sTableName]);
        } catch (DynamoDbException $e) {
            throw new TableManagerException("Can not get table information for $sTableName: " . $e->getMessage());
        }
        return $oResponse;
    }

    public function hasIndexBeenCreated(TableDefinition $oTableDefinition,
                                        string $sIndexName): bool
    {
        $this->_waitUntilTableIsReady($oTableDefinition);
        $oCurrentTableDefinition = $this->getTableDefinitionFromExistingTable($oTableDefinition->getTableName());
        return isset($oCurrentTableDefinition->getIndexDefinitions()[$sIndexName]);
    }

    public function isTableBusy(string $sTableName): bool
    {
        $bIndexIsBusy = false;
        $aTableStatus = $this->getTableInfo($sTableName);
        $bTableIsBusy = $aTableStatus['Table']['TableStatus'] != 'ACTIVE';
        foreach ($aTableStatus['Table']['GlobalSecondaryIndexes'] ?? [] as $aGSIDefinition) {
            if ($aGSIDefinition['IndexStatus'] != 'ACTIVE') {
                $bIndexIsBusy = true;
            }
        }
        return $bTableIsBusy || $bIndexIsBusy;
    }

    public function getTableDefinitionFromExistingTable(string $sTableName): TableDefinition
    {
        $aTableInfo = $this->getTableInfo($sTableName);
        $aAttributeDefinitionTypes = array_flip(self::ATTRIBUTETYPESMAPPING);
        $aSchema = [];
        $sPrimaryHashKey = '';
        $sPrimarySortKey = null;
        $aIndexes = [];
        $iPriority = 0;

        //Interpret the Schema
        foreach ($aTableInfo['Table']['AttributeDefinitions'] as $aAttributeDefinition) {
            $sAttributeName = $aAttributeDefinition['AttributeName'];
            $sAttributeType = $aAttributeDefinition['AttributeType'];
            $aSchema[$sAttributeName] = $aAttributeDefinitionTypes[$sAttributeType];
        } 

        //Interpret the primary key(s)
        foreach ($aTableInfo['Table']['KeySchema'] as $aPrimaryKeyDefinition) {
            if ($aPrimaryKeyDefinition['KeyType'] == 'HASH') {
                $sPrimaryHashKey = $aPrimaryKeyDefinition['AttributeName'];
            } else if ($aPrimaryKeyDefinition['KeyType'] == 'RANGE') {
                $sPrimarySortKey = $aPrimaryKeyDefinition['AttributeName'];
            }
        }
        $oTableDefinition = new TableDefinition($sTableName, $aSchema);

        $oTableDefinition->addIndex(new IndexDefinition(
            $sPrimaryHashKey,
            $sPrimarySortKey,
            'PrimaryKey',
            'ALL',
            true,
            true,
            $iPriority++
        ));

        //Interpret the global indexes
        foreach (($aTableInfo['Table']['GlobalSecondaryIndexes'] ?? []) as $aGSIDefinition) {
            if ($aGSIDefinition['IndexStatus'] != 'ACTIVE') {
                continue;
            }

            $sIndexName = $aGSIDefinition['IndexName'];
            $sProjectionType = $aGSIDefinition['Projection']['ProjectionType'];
            $sIndexHashKey = '';
            $sIndexSortKey = null;

            //Interpret the keys for the index
            foreach ($aGSIDefinition['KeySchema'] as $aGSIKeysDefinitions) {

                if ($aGSIKeysDefinitions['KeyType'] == 'HASH') {
                    $sIndexHashKey = $aGSIKeysDefinitions['AttributeName'];
                } else if ($aGSIKeysDefinitions['KeyType'] == 'RANGE') {
                    $sIndexSortKey = $aGSIKeysDefinitions['AttributeName'];
                }

            }

            $oTableDefinition->addIndex(new IndexDefinition(
                $sIndexHashKey,
                $sIndexSortKey,
                $sIndexName,
                'ALL',
                true,
                false,
                $iPriority++
            ));
        }

        //TODO: Add here LocalSecondaryIndexes
        return $oTableDefinition;
    }

    public function createTable(string $sTableName, 
                                string $sPrimaryHashKey, 
                                string $sPrimarySortKey = null, 
                                array $aSchema, 
                                array $aIndexes = []): TableDefinition
    {

        //Create here table definition
        $oTableDefinition = new TableDefinition($sTableName,
                                                $aSchema);

        $iPriority = 0;
        $oTableDefinition->addIndex(new IndexDefinition(
            $sPrimaryHashKey,
            $sPrimarySortKey,
            'PrimaryKey',
            'ALL',
            true,
            true,
            $iPriority++
        ));

        foreach ($aIndexes as $aIndex) {
            $oTableDefinition->addIndex(new IndexDefinition(
                $aIndex['HashKey'],
                $aIndex['SortKey'] ?? null,
                $aIndex['IndexName'] ?? $this->_generateIndexName($aIndex['HashKey'], $aIndex['SortKey'] ?? null),
                $aIndex['ProjectionType'] ?? 'ALL',
                $aIndex['IsGlobal'] ?? true,
                false,
                $aIndex['Priority'] ?? $iPriority++
            ));
        }
        return $this->createTableFromTableDefinition($oTableDefinition);
    }

    private function _generateIndexName(string $sHashKeyAttributeName,
                                        ?string $sSortKeyAttributeName): string 
    {
        return $sHashKeyAttributeName . ((!empty($sSortKeyAttributeName)) 
                    ? '-' . $sSortKeyAttributeName : '') . '_Index';
    }

    public function createTableFromTableDefinition(TableDefinition $oTableDefinition): TableDefinition
    {
        $aParams = $this->_createTableParamsForDynamoDb($oTableDefinition);

        try {
            $oResponse = $this->oConnection->getClient()->createTable($aParams);
        } catch (DynamoDbException $e) {
            throw new TableManagerException("Can not create table '{$oTableDefinition->getTableName()}': " . $e->getMessage());
        }
        return $oTableDefinition;
    }

    public function createIndexFromIndexDefinition(TableDefinition $oTableDefinition,
                                                    IndexDefinition $oIndexDefinition): void
    {
        $this->_waitUntilTableIsReady($oTableDefinition);
        $aParams = [
            'TableName'                     => $oTableDefinition->getTableName(),
            'AttributeDefinitions'          => $this->_createAttributeDefinitionsForDynamoDb($oTableDefinition,
                                                    [$oIndexDefinition->getHashKey(), $oIndexDefinition->getSortKey()]),
            'GlobalSecondaryIndexUpdates'   => 
            [
                [
                    'Create'                => 
                        $this->_createIndexParamsForDynamoDb(
                            $oIndexDefinition->getHashKey(),
                            $oIndexDefinition->getSortKey(),
                            $oIndexDefinition->getProjectionType(),
                            $oIndexDefinition->getIndexName()
                        )
                ]
            ],
        ];

        try {
            $oResponse = $this->oConnection->getClient()->updateTable($aParams);
        } catch (DynamoDbException $e) {
            throw new TableManagerException("Can not create index '{$oIndexDefinition->getIndexName()}' for 
                table '{$oTableDefinition->getTableName()}': " . $e->getMessage());
        }
    }

    public function deleteIndex(TableDefinition $oTableDefinition, string $sIndexName): void
    {
        $this->_waitUntilTableIsReady($oTableDefinition);
        $aAttributeDefinitions = 
        $aParams = [
            'TableName'                     => $oTableDefinition->getTableName(),
            'GlobalSecondaryIndexUpdates'   => [
                [
                    'Delete'                => 
                    [
                        'IndexName'         => $sIndexName
                    ]
                ]
            ]
        ];
        try {
            $oResponse = $this->oConnection->getClient()->updateTable($aParams);
        } catch (DynamoDbException $e) {
            throw new TableManagerException("Can not update table '{$oTableDefinition->getTableName()}':
                                          {$e->getMessage()}");
        }
    }

    private function _waitUntilTableIsReady(TableDefinition $oTableDefinition): void
    {
        while ($this->isTableBusy($oTableDefinition->getTableName())) {
            sleep(1);
        }
    }

    private function _createTableParamsForDynamoDb(TableDefinition $oTableDefinition): array   
    {
        $aTableParams = [];
        $aGlobalIndexParams = [];
        $aLocalIndexParams = [];
        $aAttributeDefinitions = [];

        //TODO: Local indexes not implemented yet
        
        foreach ($oTableDefinition->getIndexDefinitions() as $oIndex) {
            if (!$oIndex->isPrimaryKey()) {
                if ($oIndex->isGlobal()) {
                    $aGlobalIndexParams[] = $this->_createIndexParamsForDynamoDb($oIndex->getHashKey(), 
                                                                                $oIndex->getSortKey(), 
                                                                                $oIndex->getProjectionType(), 
                                                                                $oIndex->getIndexName());
                } else {
                    $aLocalIndexParams[] = $this->_createIndexParamsForDynamoDb($oIndex->getHashKey(), 
                                                                                $oIndex->getSortKey(), 
                                                                                $oIndex->getProjectionType(), 
                                                                                $oIndex->getIndexName());
                }
            }
        }

        $aTableParams = [
            'TableName'                 => $oTableDefinition->getTableName(),
            'KeySchema'                 => $this->_createKeyParamsForDynamoDb($oTableDefinition->getPrimaryHashKey(), $oTableDefinition->getPrimarySortKey(), 'PrimaryKey'),
            'ProvisionedThroughput'     => $this->_createProvisionedThroughputParamsForDynamoDb(10, 5),
        ];

        $aTableParams['AttributeDefinitions'] = $this->_createAttributeDefinitionsForDynamoDb($oTableDefinition,
                                                                $oTableDefinition->getIndexedAttributes());

        if (!empty($aGlobalIndexParams)) {
            $aTableParams['GlobalSecondaryIndexes'] = $aGlobalIndexParams;
        }

        if (!empty($aLocalIndexParams)) {
            $aTableParams['LocalSecondaryIndexes'] = $aLocalIndexParams;
        }

        return $aTableParams;
    }

    private function _createAttributeDefinitionsForDynamoDb(TableDefinition $oTableDefinition,
                                                            array $aAttributeNames)
    {
        $aAttributeDefinitions = [];
        foreach ($aAttributeNames as $sAttributeName) {
            if (!empty($sAttributeName)) {
                $aAttributeDefinitions[] = [
                    'AttributeName' => $sAttributeName,
                    'AttributeType' => $oTableDefinition->getDynamoTypeForAttribute($sAttributeName),
                ];
            }
        }
        return $aAttributeDefinitions;
    }

    private function _createIndexParamsForDynamoDb(string $sHashAttributeName, 
                                            string $sSortAttributeName = null, 
                                            string $sProjectionType, 
                                            string $sIndexName = null): array
    {
        $aIndexParams = [
            'IndexName'             => $sIndexName,
            'Projection'            => ['ProjectionType' => $sProjectionType],
            'ProvisionedThroughput' => $this->_createProvisionedThroughputParamsForDynamoDb(10, 5),
            'KeySchema'             => $this->_createKeyParamsForDynamoDb($sHashAttributeName, $sSortAttributeName, $sIndexName)
        ];
        return $aIndexParams;
    }

    //Null is used for primary keys
    private function _createKeyParamsForDynamoDb(string $sHashAttributeName, 
                                            string $sSortAttributeName = null, 
                                            string $sIndexName): array
    {
        $aKeyParams = [];
        foreach (['HASH' => $sHashAttributeName, 'RANGE' => $sSortAttributeName] as $sKeyType => $sAttributeName) {
            if (!empty($sAttributeName)) {
                $aKeyParams[] = [
                    'AttributeName'     => $sAttributeName,
                    'KeyType'           => $sKeyType,
                ];
            }
        }
        return $aKeyParams;
    }

    private function _createProvisionedThroughputParamsForDynamoDb(int $iReadUnits, 
                                                            int $iWriteUnits): array
    {
        return [
            'ReadCapacityUnits'     => $iReadUnits,
            'WriteCapacityUnits'    => $iWriteUnits,
        ];
    }
}
