<?php
namespace EoneoPay\DatabaseRepos;
class ORCriteria extends Criteria
{
    //Treat OR-Criteria always as nested, because it's never valid to use indexes on them
    protected $bIsNested = true;

    public function getLogicalOperator(): string
    {
        return ' OR ';
    }
}
