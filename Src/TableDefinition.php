<?php
namespace EoneoPay\DatabaseRepos;

use EoneoPay\DatabaseRepos\Exceptions\{AttributeValueTypeMismatchException,
        AttributeValueIsNullForIndexedAttributeException,
        UnsupportedTypeException,
        IndexedAttributeNameNotInSchema,
        IndexDoesNotExistException};

class TableDefinition implements \JsonSerializable
{
    const ATTRIBUTETYPESMAPPING = [
        'integer'   => 'N',
        'array'     => 'M',
        'string'    => 'S',
        'double'    => 'N',
        'boolean'   => 'BOOL',
        'object'    => 'M',
        'list'      => 'L',
        'null'      => 'NULL',
    ];

    private $sTableName;
    private $aIndexDefinitions = [];

    private $sPrimaryHashKey = null;
    private $sPrimarySortKey = null;

    private $aIndexedAttributes = [];
    private $aAttributeDefinitions = [];

    /**
     * Creates a TableDefinition object from a JSON-representation
     * 
     * @param  string $sJson JSON-string
     * @return  TableDefinition 
     */
    static public function createFromJson(string $sJson): TableDefinition
    {
        $aObject = json_decode($sJson, true);
        $oTableDefinition = new TableDefinition(
            $aObject['sTableName'],
            $aObject['aAttributeDefinitions']
        );

        foreach ($aObject['aIndexDefinitions'] as $aIndexDefinition) {
            $oIndexDefinition = new IndexDefinition(
                $aIndexDefinition['sHashKey'],
                $aIndexDefinition['sSortKey'],
                $aIndexDefinition['sIndexName'],
                $aIndexDefinition['sProjectionType'],
                $aIndexDefinition['bIsGlobal'],
                $aIndexDefinition['bIsPrimaryKey'],
                $aIndexDefinition['iPriority']
            );
            $oTableDefinition->addIndex($oIndexDefinition);
        }
        return $oTableDefinition;
    }

    /**
     * Serialize this object to a JSON-representation
     * 
     * @return  string JSON-representation of this object
     */
    public function jsonSerialize(): array 
    {
        return [
            'sTableName'                => $this->sTableName,
            'aIndexDefinitions'         => $this->aIndexDefinitions,
            'aAttributeDefinitions'     => $this->aAttributeDefinitions,
        ];
    }

    /**
     * Constructs an instance of TableDefinition
     * 
     * @param  string $sTableName Name of the table this object represents
     * @param  array  $aIndexDefinitions An array of arrays which represent
     *                                           the indexes
     * @param  array $aAttributeDefinitions An array where the keys are the attribute fields
     *                                      and the values the types (string, integer, ...)
     */
    public function __construct(string $sTableName,
                                array $aAttributeDefinitions)
    {
        $this->sTableName = $sTableName;
        $this->aAttributeDefinitions = $aAttributeDefinitions;
    }

    public function addIndex(IndexDefinition $oIndexDefinition): void 
    {
        $this->aIndexedAttributes[] = $oIndexDefinition->getHashKey();

        if (empty($this->aAttributeDefinitions[$oIndexDefinition->getHashKey()])) {
            throw new IndexedAttributeNameNotInSchema("Hash key '{$oIndexDefinition->getHashKey()}' is not defined in schema");
        }

        if (!is_null($oIndexDefinition->getSortKey())) {
            if (empty($this->aAttributeDefinitions[$oIndexDefinition->getSortKey()])) {
                throw new IndexedAttributeNameNotInSchema("Sort key '{$oIndexDefinition->getSortKey()}' is not defined in schema");
            }            
            $this->aIndexedAttributes[] = $oIndexDefinition->getSortKey();
        }
        if ($oIndexDefinition->isPrimaryKey()) {
            $this->sPrimaryHashKey = $oIndexDefinition->getHashKey();
            $this->sPrimarySortKey = $oIndexDefinition->getSortKey();
        }

        $this->aIndexDefinitions[$oIndexDefinition->getIndexName()] = $oIndexDefinition;
    }

    public function deleteIndex(string $sIndexName) 
    {
        if (empty($this->aIndexDefinitions[$sIndexName])) {
            throw new IndexDoesNotExistException("Can not delete index '$sIndexName', it does not exist");
        }
        unset($this->aIndexDefinitions[$sIndexName]);
    }

    public function addAttribute(string $sAttributeName,
                                string $sAttributeType): void
    {
        $this->aAttributeDefinitions[$sAttributeName] = $sAttributeType;
    }

    public function getTableName(): string
    {
        return $this->sTableName;
    }

    public function getIndexedAttributes(): array 
    {
        return array_unique($this->aIndexedAttributes);
    }

    public function getAttributeDefinitions(): array
    {
        return $this->aAttributeDefinitions;
    }

    public function getIndexDefinitions(): array
    {
        return $this->aIndexDefinitions;
    }

    public function getPrimaryHashKey(): string
    {
        return $this->sPrimaryHashKey;
    }

    public function getPrimarySortKey(): ?string
    {
        return $this->sPrimarySortKey;
    }

    public function mergeIntoSchema(array $aAttributeDefinitions): void
    {
        $this->aAttributeDefinitions = array_merge($this->aAttributeDefinitions, $aAttributeDefinitions);
    }

    public function getTypeForAttribute($sAttributeName): ?string
    {
        return $this->aAttributeDefinitions[$sAttributeName] ?? null;
    }

    public function getDynamoTypeForAttribute($sAttributeName): ?string
    {
        return ($this->getTypeForAttribute($sAttributeName)) 
            ? self::ATTRIBUTETYPESMAPPING[$this->getTypeForAttribute($sAttributeName)]
            : null;
    }

    public function isAttributeIndexed(string $sAttributeName): bool
    {
        return in_array($sAttributeName, $this->aIndexedAttributes);
    }

    public function getDynamoFieldTypeAndTransformValue(string $sAttributeName, 
                                                        $mAttributeValue, 
                                                        bool $bUseSchema): array
    {
        //At this point, the schema is only used for the first level of an object. Deeper within the object
        //we determine the datatypes automatically

        if ($bUseSchema && $this->getTypeForAttribute($sAttributeName)) {
            //A scalar attribute value cannot be converted to a non-scalar attribute value or the other 
            //way around
            if (is_scalar($mAttributeValue) 
                xor $this->_isScalarAttribute($sAttributeName)) {
                throw new AttributeValueTypeMismatchException("Cannot convert value for attribute $sAttributeName");
            }
            
            if ($this->getTypeForAttribute($sAttributeName) == 'string'
                && strlen($mAttributeValue) == 0
                && $this->isAttributeIndexed($sAttributeName)) {
                throw new AttributeValueIsNullForIndexedAttributeException("The length of an attribute value for indexed attribute $sAttributeName can not be 0.");
            }
            //Convert type to schema type
            settype($mAttributeValue, $this->getTypeForAttribute($sAttributeName));
        }

        //Set the DynamoType 
        $sDynamoType = self::ATTRIBUTETYPESMAPPING[$this->_getTypeExtended($mAttributeValue)] ?? null;

        if (is_null($sDynamoType)) {
            throw new UnsupportedTypeException("Data type {$this->_getTypeExtended($mAttributeValue)} given for $sAttributeName is not supported");
        }

        //Perform some transformations
        switch ($this->_getTypeExtended($mAttributeValue)) {
            //If strlen == 0 continue, and make it a NULL
            case 'string':
                if (strlen($mAttributeValue) != 0) {
                    break;
                }
            case 'null':
                $sDynamoType = 'NULL';
                $mAttributeValue = true;
                break;
            case 'integer':
            case 'double':
                $mAttributeValue = (string) $mAttributeValue;
        }

        return [
            'PropertyKey'         => $sAttributeName,
            'DynamoType'          => $sDynamoType,
            'TransformedValue'    => $mAttributeValue,
        ];
    }

    private function _isScalarAttribute(string $sAttributeName): bool 
    {
        return (!in_array($this->getTypeForAttribute($sAttributeName), ['list', 'resource', 'object', 'array', 'null']));
    }

    private function _getTypeExtended($mAttributeValue): string 
    {
        $sType = gettype($mAttributeValue);
        if ($sType == 'array' && $this->_isSequentialArray($mAttributeValue)) {
            return 'list';
        }
        return strtolower($sType);
    }

    private function _isSequentialArray(array $aArray): bool
    {
        if ([] === $aArray) {
            return false;
        }
        return array_keys($aArray) === range(0, count($aArray) - 1);
    }
}
