<?php
namespace EoneoPay\DatabaseRepos;

class TransactionRepository extends BaseRepository
{
    protected $oTableDefinition;

    public function __construct(Connection $oConnection = null, string $sTableName)
    {
        $this->oTableDefinition = new TableDefinition($sTableName, [

        ],
        [
            'TransactionID'     => 'string',
            'Status'            => 'string',
            'HasBeenCommitted'  => 'boolean',
            'TimestampStarted'  => 'integer',
            'TimestampUpdated'  => 'integer',
        ], 'id');

        parent::__construct($oConnection, 
                            Transaction::class, 
                            $this->oTableDefinition);
    }

    public function getTableDefinition()
    {
        return $this->oTableDefinition;
    }
}
