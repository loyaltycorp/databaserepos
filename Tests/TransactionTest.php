<?php
use EoneoPay\DatabaseRepos\{Connection,SampleRepository,TableManager,ANDCriteria,
    ORCriteria,Transaction,TransactionRepository};
use EoneoPay\DatabaseRepos\Exceptions\ConditionalCheckFailedException;

class TransactionTest extends TestCase 
{
    //Transactions have not been implemented yet.
    // public function testCanUserAddSingleNewObjectToTransactionAndCommit()
    // {
    //     $oTableManager = new TableManager($this->oConnection);

    //     foreach (['Sample_Repository', 'Transactions', 'TransactionObjects'] as $sTableName) {
    //         if($oTableManager->doesTableExist($sTableName)) {
    //             $oTableManager->deleteTable($sTableName);
    //         }
    //     }

    //     $oTransactionRepository = new TransactionRepository($oConnection, 'Transactions');
    //     $oTableManager->createTableFromTableDefinition($oTransactionRepository->getTableDefinition());
        
    //     $oTransactionObjectRepository = new TransactionObjectRepository($oConnection, 'TransactionsObjects');
    //     $oTableManager->createTableFromTableDefinition($oTransactionRepository->getTableDefinition());
    // }

    public function testCanUserUseConditionalWrites()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'        => 'integer',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = (object) [
            'id'        => 123,
            'name'      => 'Test Name',
            'condition' => 'no'
        ];
        $oSampleRepository->saveObject($oObject);

        $this->expectException(ConditionalCheckFailedException::class);
        $oSampleRepository->saveObject($oObject, true, ['condition' => 'yes']);        
    }

    public function testCanUserUseBatchWrites()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'        => 'integer',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObjectA = (object) [
            'id'        => 123,
            'name'      => 'Test Name1',
        ];

        $oObjectB = (object) [
            'id'        => 456,
            'name'      => 'Test Name2',
        ];

        $oSampleRepository->saveMultipleObjects([
            $oObjectA,
            $oObjectB,
        ]);

        $oRetrievedObject = $oSampleRepository->getObjectById(123);
        $this->assertEquals($oObjectA, $oRetrievedObject);

        $oRetrievedObject = $oSampleRepository->getObjectById(456);
        $this->assertEquals($oObjectB, $oRetrievedObject);
    }
}
