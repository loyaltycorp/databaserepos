<?php
use EoneoPay\DatabaseRepos\{Connection,SampleRepository,TableManager,ANDCriteria,ORCriteria};
use EoneoPay\DatabaseRepos\Exceptions\SaveObjectException;

class LimitOffsetTest extends TestCase 
{
    public function testCanUserUseLimitAndOffset() 
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', 'sort', [
            'id'        => 'integer',
            'sort'       => 'string',
        ], []);

        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition, null, null);

        for ($i = 0; $i < 21; $i++) {
            $oObject = new \stdClass;
            $oObject->id = $i;
            $oObject->sort = range('A', 'Z')[$i];
            $oObject->name = $this->oFaker->name;
            $oObject->streetName = $this->oFaker->streetName;
            $oObject->postcode = 3001;
            $oSampleRepository->saveObject($oObject);
        }
        $aCollection = [];
        do {
            $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ], true, 10, count($aCollection));
            $aCollection = array_merge($aItems, $aCollection);
        } while (count($aItems) > 0);

        $this->assertCount(21, $aCollection);
        $this->assertEquals('SCAN', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
    }

    // public function testCanUserUseLimitAndOffsetUsingNextVector() 
    // {

    //     $this->oTableManager = new TableManager($this->oConnection);

    //     if($this->oTableManager->doesTableExist($this->sTableName)) {
    //         $this->oTableManager->deleteTable($this->sTableName);
    //     }
    //     $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', 'sort', [
    //         'id'        => 'integer',
    //         'sort'       => 'string',
    //     ], []);

    //     $oPaginator = [];
    //     $oPaginatorClosure = function(array $aPaginatorData) use (&$oPaginator) {
    //         $oPaginator = $aPaginatorData;
    //     };

    //     $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition, null, $oPaginatorClosure);

    //     for ($i = 0; $i < 21; $i++) {
    //         $oObject = new \stdClass;
    //         $oObject->id = $i;
    //         $oObject->sort = range('A', 'Z')[$i];
    //         $oObject->name = $this->oFaker->name;
    //         $oObject->streetName = $this->oFaker->streetName;
    //         $oObject->postcode = 3001;
    //         $oSampleRepository->saveObject($oObject);
    //     }
        
    //     $aItems = $oSampleRepository->getObjectsByANDCriteria([
    //     ], true, 10, 0);
    //     print_r($oPaginator);


    //     $this->assertEquals('SCAN', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
    // }
}
