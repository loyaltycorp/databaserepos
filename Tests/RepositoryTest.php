<?php
use EoneoPay\DatabaseRepos\{SampleRepository,TableManager,ANDCriteria,ORCriteria};
use EoneoPay\DatabaseRepos\Exceptions\SaveObjectException;

class RepositoryTest extends TestCase 
{

    public function testCanUserCreateAndGetSingleObjectsInRepository() 
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'        => 'integer',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = (object) [
            'id'        => 123,
            'name'      => 'Test Name',
            'address'   => 'Test Address'
        ];
        
        $oSampleRepository->saveObject($oObject);

        $oRetrievedObject = $oSampleRepository->getObjectById(123);
        $this->assertEquals('GETITEM', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));

        $this->assertEquals($oRetrievedObject->id, $oObject->id);
        $this->assertEquals($oRetrievedObject->name, $oObject->name);
    }

    public function testCanUserListItemsBasedOnSingleCriteriumUsingNoIndexedFields()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'        => 'integer',
            'name'      => 'string',
            'streetName'=> 'string',
            'postcode'  => 'integer',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 5; $i++) {
            $oObject = new \stdClass;
            $oObject->id = $this->oFaker->randomNumber(5);
            $oObject->name = $this->oFaker->name;
            $oObject->streetName = $this->oFaker->streetName;
            $oObject->postcode = 3001;
            $oSampleRepository->saveObject($oObject);
        }

        for ($i = 0; $i < 8; $i++) {
            $oObject = new \stdClass;
            $oObject->id = $this->oFaker->randomNumber(5);
            $oObject->name = $this->oFaker->name;
            $oObject->streetName = $this->oFaker->streetName;
            $oObject->postcode = 3000;
            $oSampleRepository->saveObject($oObject);
        }

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            'postcode' => 3000,
        ]);

        $this->assertCount(8, $aItems);
        $this->assertEquals('SCAN', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            'postcode' => 3001,
        ]);

        $this->assertCount(5, $aItems);
        $this->assertEquals('SCAN', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
    }

    public function testCanUserListItemsBasedOnMultipleCriteriaUsingNoIndexedFields()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'        => 'integer',
            'name'      => 'string',
            'streetName'=> 'string',
            'state'     => 'string',
            'postcode'  => 'integer',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 8; $i++) {
            $oObject = new \stdClass;
            $oObject->id = $this->oFaker->randomNumber(5);
            $oObject->name = $this->oFaker->name;
            $oObject->streetName = $this->oFaker->streetName;
            $oObject->postcode = 3001;
            $oObject->state = 'VIC';
            $oSampleRepository->saveObject($oObject);
        }

        for ($i = 0; $i < 3; $i++) {
            $oObject = new \stdClass;
            $oObject->id = $this->oFaker->randomNumber(5);
            $oObject->name = $this->oFaker->name;
            $oObject->streetName = $this->oFaker->streetName;
            $oObject->postcode = 3001;
            $oObject->state = 'QLD';
            $oSampleRepository->saveObject($oObject);
        }

        for ($i = 0; $i < 5; $i++) {
            $oObject = new \stdClass;
            $oObject->id = $this->oFaker->randomNumber(5);
            $oObject->name = $this->oFaker->name;
            $oObject->streetName = $this->oFaker->streetName;
            $oObject->postcode = 3000;
            $oObject->state = 'VIC';
            $oSampleRepository->saveObject($oObject);
        }

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            'postcode' => 3001,
            'state'    => 'VIC',
        ]);

        $this->assertCount(8, $aItems);
        $this->assertEquals('SCAN', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
    }

    public function testCanUserListItemsBasedOnMultipleCriteriaUsingIndexedFields()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'        => 'integer',
            'name'      => 'string',
            'streetName'=> 'string',
            'state'     => 'string',
            'postcode'  => 'integer',
        ], [
            ['HashKey' => 'postcode', 'SortKey' => 'streetName']
        ]);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 8; $i++) {
            $oObject = new \stdClass;
            $oObject->id = $this->oFaker->randomNumber(5);
            $oObject->name = $this->oFaker->name;
            $oObject->streetName = $this->oFaker->streetName;
            $oObject->postcode = 3001;
            $oObject->state = 'VIC';
            $oSampleRepository->saveObject($oObject);
        }

        for ($i = 0; $i < 3; $i++) {
            $oObject = new \stdClass;
            $oObject->id = $this->oFaker->randomNumber(5);
            $oObject->name = $this->oFaker->name;
            $oObject->streetName = $this->oFaker->streetName;
            $oObject->postcode = 3001;
            $oObject->state = 'QLD';
            $oSampleRepository->saveObject($oObject);
        }

        for ($i = 0; $i < 5; $i++) {
            $oObject = new \stdClass;
            $oObject->id = $this->oFaker->randomNumber(5);
            $oObject->name = $this->oFaker->name;
            $oObject->streetName = $this->oFaker->streetName;
            $oObject->postcode = 3000;
            $oObject->state = 'VIC';
            $oSampleRepository->saveObject($oObject);
        }

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            'postcode' => 3001,
            'state'    => 'VIC',
        ]);
        $this->assertCount(8, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            'postcode' => 3000,
            'state'    => 'VIC',
        ]);
        $this->assertCount(5, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            'postcode' => 3001,
        ]);
        $this->assertCount(11, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
    }

    public function testCanUserCreateAndGetSingleComplexObjectsInRepository() 
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'        => 'integer',
            'name'      => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = (object) [
            'id'        => 123,
            'name'      => $this->oFaker->name,
            'address'   => [
                'street'        => [
                    'streetName'   => $this->oFaker->streetName,
                    'streetNumber' => $this->oFaker->numberBetween(10,100),
                ],
                'postcode'      => $this->oFaker->postCode,
            ],
            'phoneNumbers' => [
                '12345',
                '56789',
                '55667',
            ],
            'nullItem'      => NULL,
            'booleanFalse'  => false,
            'booleanTrue'   => true,
            'isZero'        => 0,
            'emptyString'   => '',
            'spaceString'   => ' ',
            'otherNames' => [
                100 => 'name1',
                123 => 'name2',
                500 => 'name3',
            ]
        ];

        $oSampleRepository->saveObject($oObject);

        $oRetrievedObject = $oSampleRepository->getObjectById(123);

        $this->assertEquals($oObject, $oRetrievedObject);
    }

    public function testCanUserListItemsAndQueryThemForwardsSortedUsingSecondarySortKey()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'integer',
            'sortedNumber'  => 'integer',
            'sameName'      => 'string',
        ], [
            ['HashKey' => 'sameName', 'SortKey' => 'sortedNumber']
        ]);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 10; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->sortedNumber  = $this->oFaker->randomNumber(5);
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            'sameName'  => 'SameName',
        ], false);
        $this->assertCount(10, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));

        $oLastItem = null;
        foreach ($aItems as $oItem) {
            if ($oLastItem !== null) {
                $this->assertLessThanOrEqual($oItem->sortedNumber, $oLastItem->sortedNumber);
            }
            $oLastItem = $oItem;
        }
    }

    public function testCanUserListItemsAndQueryThemBackwardsSortedUsingSecondarySortKey()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'integer',
            'sortedNumber'  => 'integer',
            'sameName'      => 'string',
        ], [
            ['HashKey' => 'sameName', 'SortKey' => 'sortedNumber']
        ]);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 10; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->sortedNumber  = $this->oFaker->randomNumber(5);
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            'sameName'  => 'SameName',
        ], true);
        $this->assertCount(10, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));

        $oLastItem = null;
        foreach ($aItems as $oItem) {
            if ($oLastItem !== null) {
                $this->assertGreaterThanOrEqual($oItem->sortedNumber, $oLastItem->sortedNumber);
            }
            $oLastItem = $oItem;
        }
    }

    public function testCanUserQuerySingleItemUsingHashAndSortIndexKey()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'integer',
            'sortedNumber'  => 'integer',
            'sameName'      => 'string',
        ], [
            ['HashKey' => 'sameName', 'SortKey' => 'sortedNumber']
        ]);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->sortedNumber  = 123;
        $oObject->sameName      = 'SameName';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            'sameName'      => 'SameName',
            'sortedNumber'  => 123,
        ], false);

        $this->assertCount(1, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
        $this->assertEquals('sortedNumber', $oSampleRepository->getDebugInfo('LAST_USED_SORT_KEY'));
    }

    public function testCanUserQuerySingleItemUsingHashAndSortIndexKeyPlusOtherField()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'integer',
            'sortedNumber'  => 'integer',
            'sameName'      => 'string',
            'otherField'    => 'string',
        ], [
            ['HashKey' => 'sameName', 'SortKey' => 'sortedNumber']
        ]);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->sortedNumber  = 123;
        $oObject->sameName      = 'SameName';
        $oObject->otherField    = 'OtherField';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            'sameName'      => 'SameName',
            'sortedNumber'  => 123,
            'otherField'    => 'OtherField',
        ], false);

        $this->assertCount(1, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
        $this->assertEquals('sortedNumber', $oSampleRepository->getDebugInfo('LAST_USED_SORT_KEY'));
    }

    public function testCanUserQuerySingleItemUsingHashAndSortUsingHigherThanAndLowerThanOperator()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'integer',
            'randomNumber'  => 'integer',
            'sameName'      => 'string',
        ], [
            ['HashKey' => 'sameName', 'SortKey' => 'randomNumber']
        ]);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 15; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        for ($i = 0; $i < 6; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1,999);
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['sameName',     '=', 'SameName'],
            ['randomNumber', '>', 1000],
        ], false);

        $this->assertCount(15, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
        $this->assertEquals('randomNumber', $oSampleRepository->getDebugInfo('LAST_USED_SORT_KEY'));

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['sameName',     '=', 'SameName'],
            ['randomNumber', '<', 1000],
        ], false);

        $this->assertCount(6, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
        $this->assertEquals('randomNumber', $oSampleRepository->getDebugInfo('LAST_USED_SORT_KEY'));
    }

    public function testCanUserQuerySingleItemUsingHashAndSortUsingBetweenInOneQuery()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'integer',
            'randomNumber'  => 'integer',
            'sameName'      => 'string',
        ], [
            ['HashKey' => 'sameName', 'SortKey' => 'randomNumber']
        ]);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 6; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1,999);
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        for ($i = 0; $i < 12; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['sameName',     '=', 'SameName'],
            ['randomNumber', '>=', 1000],
            ['randomNumber', '<=', 10000],
        ], false);

        $this->assertCount(12, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
        $this->assertEquals('randomNumber', $oSampleRepository->getDebugInfo('LAST_USED_SORT_KEY'));
    }

    public function testCanUserQuerySingleItemUsingPrimaryHashAndSort()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', 'merchant_id', [
            'id'            => 'integer',
            'merchant_id'   => 'integer',
            'name'          => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->merchant_id   = $this->oFaker->numberBetween(1,999);
        $oObject->name          = 'Name';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['id',          '=', $oObject->id],
            ['merchant_id', '=', $oObject->merchant_id],
        ], false);

        $this->assertCount(1, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
    }

    public function testCanUserQuerySingleItemUsingPrimaryHashAndSortUsingHigherThanAndLowerThanOperator()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'sameName', 'randomNumber', [
            'randomNumber'  => 'integer',
            'sameName'      => 'string',
        ], [
            ['HashKey' => 'sameName', 'SortKey' => 'randomNumber']
        ]);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 15; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        for ($i = 0; $i < 6; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1,999);
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['sameName',     '=', 'SameName'],
            ['randomNumber', '>', 1000],
        ], false);

        $this->assertCount(15, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
        $this->assertEquals('randomNumber', $oSampleRepository->getDebugInfo('LAST_USED_SORT_KEY'));

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['sameName',     '=', 'SameName'],
            ['randomNumber', '<', 1000],
        ], false);

        $this->assertCount(6, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
        $this->assertEquals('randomNumber', $oSampleRepository->getDebugInfo('LAST_USED_SORT_KEY'));
    }

    public function testCanUserQuerySingleItemUsingPrimaryHash()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', 'merchant_id', [
            'id'            => 'integer',
            'merchant_id'   => 'integer',
            'name'          => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->merchant_id   = $this->oFaker->numberBetween(1,999);
        $oObject->name          = 'Name';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['id',          '=', $oObject->id],
        ], false);

        $this->assertCount(1, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
    }

    public function testCanUserQueryMultipleItemUsingScanActionAndHigherThanAndLowerThanOperators()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'integer',
            'randomNumber'  => 'integer',
            'sameName'      => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 15; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        for ($i = 0; $i < 6; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1,999);
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['sameName',     '=', 'SameName'],
            ['randomNumber', '>', 1000],
        ], false);

        $this->assertCount(15, $aItems);
        $this->assertEquals('SCAN', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['sameName',     '=', 'SameName'],
            ['randomNumber', '<', 1000],
        ], false);

        $this->assertCount(6, $aItems);
        $this->assertEquals('SCAN', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
    }

    public function testCanUserQueryMultipleItemWithoutConditionsUsingQuery()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'integer',
            'randomNumber'  => 'integer',
            'sameName'      => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 15; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
        ], false);

        $this->assertCount(15, $aItems);
        $this->assertEquals('SCAN', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
    }

    public function testCanUserFindAnItemUsingContainsOperator()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'integer',
            'randomNumber'  => 'integer',
            'sameName'      => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 15; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->randomNumber  = 123;
        $oObject->sameName      = 'OtherItem';
        $oSampleRepository->saveObject($oObject);


        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['sameName', 'contains', 'OtherI'],
        ], false);

        $this->assertCount(1, $aItems);
        $this->assertEquals($oObject, $aItems[0]);
        $this->assertEquals('SCAN', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
    }

    public function testCanUserFindAnItemUsingBeginsWithOperator()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'integer',
            'randomNumber'  => 'integer',
            'sameName'      => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 15; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->randomNumber  = 123;
        $oObject->sameName      = 'OtherItem';
        $oSampleRepository->saveObject($oObject);


        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['sameName', 'begins_with', 'OtherI'],
        ], false);

        $this->assertCount(1, $aItems);
        $this->assertEquals($oObject, $aItems[0]);
        $this->assertEquals('SCAN', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
    }

    public function testCanUserFindAnItemUsingContainsOperatorCombinedWithIndexUsingQuery()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'integer',
            'merchant_id'   => 'string',
            'randomNumber'  => 'integer',
            'sameName'      => 'string',
        ], [
            ['HashKey' => 'merchant_id']
        ]);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 15; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
            $oObject->merchant_id   = 'MerchantA';
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        for ($i = 0; $i < 15; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
            $oObject->merchant_id   = 'MerchantB';
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
        $oObject->merchant_id   = 'MerchantB';
        $oObject->sameName      = 'OtherName';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['merchant_id', '=', 'MerchantB'],
            ['sameName', 'contains', 'OtherN'],
        ], false);

        $this->assertCount(1, $aItems);
        $this->assertEquals($oObject, $aItems[0]);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
    }

    public function testCanUserFindAnItemUsingAttributeExistsOperator()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'integer',
            'randomNumber'  => 'integer',
            'sameName'      => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 2; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
            $oSampleRepository->saveObject($oObject);
        }

        for ($i = 0; $i < 21; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['sameName', 'attribute_exists', null],
        ], false);

        $this->assertCount(21, $aItems);
        $this->assertEquals('SCAN', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['sameName', 'attribute_not_exists', null],
        ], false);

        $this->assertCount(2, $aItems);
        $this->assertEquals('SCAN', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
    }


    public function testCanUserFindAnItemUsingContainsOperatorCombinedWithIndexOnContainsAttributeUsingQuery()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'integer',
            'merchant_id'   => 'string',
            'randomNumber'  => 'integer',
            'sameName'      => 'string',
        ], [
            ['HashKey' => 'merchant_id', 'SortKey' => 'sameName']
        ]);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 15; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
            $oObject->merchant_id   = 'MerchantA';
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        for ($i = 0; $i < 15; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
            $oObject->merchant_id   = 'MerchantB';
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
        $oObject->merchant_id   = 'MerchantB';
        $oObject->sameName      = 'OtherName';
        $oSampleRepository->saveObject($oObject);

        //The aim of this test is to do this request and NOT using the index since the contains
        //operator is not allowed to use any indexes.

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['merchant_id', '=', 'MerchantB'],
            ['sameName', 'contains', 'OtherN'],
        ], false);

        $this->assertCount(1, $aItems);
        $this->assertEquals($oObject, $aItems[0]);
        $this->assertEquals('SCAN', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
    }

    public function testAreIndexesUsedCorrectlyPrioritized()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'integer',
            'merchant_id'   => 'string',
            'randomNumber'  => 'integer',
            'sameName'      => 'string',
        ], [
            ['HashKey' => 'merchant_id'],
            ['HashKey' => 'sameName'],
            ['HashKey' => 'merchant_id', 'SortKey' => 'sameName'],
            ['HashKey' => 'sameName', 'SortKey' => 'merchant_id'],
        ]);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 15; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
            $oObject->merchant_id   = 'MerchantA';
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        for ($i = 0; $i < 15; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
            $oObject->merchant_id   = 'MerchantB';
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->randomNumber  = 123123;
        $oObject->merchant_id   = 'MerchantB';
        $oObject->sameName      = 'OtherName';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['merchant_id', '=', 'MerchantB'],
            ['sameName', '=', 'SameName'],
        ], false);

        $this->assertCount(15, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
        $this->assertEquals('merchant_id-sameName_Index', $oSampleRepository->getDebugInfo('LAST_USED_INDEX_NAME'));

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['sameName', '=', 'SameName'],
        ], false);

        $this->assertCount(30, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
        $this->assertEquals('sameName_Index', $oSampleRepository->getDebugInfo('LAST_USED_INDEX_NAME'));

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['merchant_id', '=', 'MerchantB'],
        ], false);

        $this->assertCount(16, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
        $this->assertEquals('merchant_id_Index', $oSampleRepository->getDebugInfo('LAST_USED_INDEX_NAME'));
    }

    public function testCanUserNotUseInvalidDataTypesToQuery()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'integer',
            'randomNumber'  => 'integer',
            'sameName'      => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        //Bla is a string, not integer as defined
        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['id', 'contains', 'bla'],
        ], false);

        $this->assertCount(0, $aItems);
    }

    public function testIsScanOperationUsedWhenUsingContainsOperatorOnPrimaryHashKey()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
            'randomNumber'  => 'integer',
            'sameName'      => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 21; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['id', 'contains', 'bla'],
        ], false);

        $this->assertCount(0, $aItems);
        $this->assertEquals('SCAN', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
    }

    public function testIsExceptionThrownWhenSavingObjectWithoutPrimaryHashKey()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
            'randomNumber'  => 'integer',
            'sameName'      => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $this->expectException(SaveObjectException::class);
        $oObject = new \stdClass;
        $oObject->sameName      = 'SameName';
        $oSampleRepository->saveObject($oObject);
    }

    public function testIsExceptionThrownWhenSavingObjectWithoutPrimarySortKey()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', 'merchant_id', [
            'id'            => 'string',
            'merchant_id'   => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $this->expectException(SaveObjectException::class);
        $oObject = new \stdClass;
        $oObject->id            = 'id';
        $oSampleRepository->saveObject($oObject);
    }

    public function testCanUserUseORCriteriaToFindMultipleObjects()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
            'randomNumber'  => 'integer',
            'sameName'      => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
        $oObject->sameName      = 'SameName';
        $oSampleRepository->saveObject($oObject);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
        $oObject->sameName      = 'SecondName';
        $oSampleRepository->saveObject($oObject);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->randomNumber  = $this->oFaker->numberBetween(1001,9999);
        $oObject->sameName      = 'ThirdName';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByCriteria(new ORCriteria([
            ['sameName', '=', 'SameName'],
            ['sameName', '=', 'SecondName'],
        ], false));

        $this->assertCount(2, $aItems);
        $this->assertEquals('SCAN', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
    }

    public function testCanUserCombineORinANDCriteriaAndUseIndex()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
            'randomNumber'  => 'integer',
            'sameName'      => 'string',
        ], [
            ['HashKey' => 'sameName'],
        ]);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        for ($i = 0; $i < 21; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->randomNumber  = $this->oFaker->numberBetween(1,9999);
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->randomNumber  = 123120;
        $oObject->sameName      = 'SameName';
        $oSampleRepository->saveObject($oObject);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->randomNumber  = 123123;
        $oObject->sameName      = 'SameName';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByCriteria(new ANDCriteria([
            ['sameName', '=', 'SameName'],
            new ORCriteria([
                ['randomNumber', '<', 10000],
                ['randomNumber', '=', 123123],
            ]),
            ['randomNumber', '>', 1]
        ], false));

        $this->assertCount(22, $aItems);
        $this->assertEquals('QUERY', $oSampleRepository->getDebugInfo('LAST_OPERATION_TYPE'));
    }

    public function testCanUserFilterOnEmptyStringWhichIsInSchema()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
            'value'         => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->value         = '';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['value',     '=', ''],
        ], false);

        $this->assertCount(1, $aItems);
        $this->assertEquals($oObject, $aItems[0]);
    }

    public function testCanUserFilterOnEmptyStringWhichIsNotInSchema()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->value         = '';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['value',     '=', ''],
        ], false);

        $this->assertCount(1, $aItems);
        $this->assertEquals($oObject, $aItems[0]);
    }

    public function testCanUserFilterOnBooleanFalseWhichIsInSchema()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
            'value'         => 'boolean',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->value         = false;
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['value',     '=', false],
        ], false);

        $this->assertCount(1, $aItems);
        $this->assertEquals($oObject, $aItems[0]);
    }

    public function testCanUserFilterOnBooleanTrueWhichIsInSchema()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
            'value'         => 'boolean',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->value         = true;
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['value',     '=', true],
        ], false);

        $this->assertCount(1, $aItems);
        $this->assertEquals($oObject, $aItems[0]);
    }

    public function testCanUserFilterOnBooleanTrueWhichIsNotInSchema()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->value         = true;
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['value',     '=', true],
        ], false);

        $this->assertCount(1, $aItems);
        $this->assertEquals($oObject, $aItems[0]);
    }

    public function testCanUserFilterOnBooleanFalseWhichIsNotInSchema()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->value         = false;
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['value',     '=', false],
        ], false);

        $this->assertCount(1, $aItems);
        $this->assertEquals($oObject, $aItems[0]);
    }

    public function testCanUserNotFilterOnBooleanFalseWhenValueIsSavedAsNull()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->value         = null;
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['value',     '=', false],
        ], false);

        $this->assertCount(0, $aItems);
    }

    public function testCanUserFilterOnNullWhenValueIsEmptyString()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->value         = '';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['value',     '=', null],
        ], false);

        $this->assertCount(1, $aItems);
    }

    public function testCanUserFilterOnEmptyStringWhenValueIsEmptyString()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->value         = '';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['value',     '=', ''],
        ], false);

        $this->assertCount(1, $aItems);
    }

    public function testCanUserFilterOnBooleanFalseWhenValueIsZero()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
            'value'         => 'boolean',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->value         = 0;
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['value',     '=', false],
        ], false);

        $this->assertCount(1, $aItems);
    }

    public function testCanUserFilterOnStringNumericWhenValueIsIntegerWithSchema()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
            'value'         => 'integer',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->value         = 123;
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['value',     '=', '123'],
        ], false);

        $this->assertCount(1, $aItems);
    }

    public function testCanUserFilterOnIntegerWhenValueIsStringWithSchema()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
            'value'         => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->value         = '123';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['value',     '=',  123],
        ], false);

        $this->assertCount(1, $aItems);
    }

    public function testCanUserFilterOnIntegerWhenValueIsStringWithoutSchema()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->value         = '123';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['value',     '=',  '123'],
        ], false);

        $this->assertCount(1, $aItems);
    }

    public function testCanUserUpdateObject()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->value         = '123';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['id',     '=',  $oObject->id],
        ], false);
        $this->assertCount(1, $aItems);
        $oRetrievedObject = $aItems[0];
        $this->assertEquals($oObject, $oRetrievedObject);

        $oRetrievedObject->value = '345';
        $oSampleRepository->saveObject($oRetrievedObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['id',     '=',  $oObject->id],
        ], false);
        $this->assertCount(1, $aItems);
        $oSecondRetrievedObject = $aItems[0];
        $this->assertEquals($oRetrievedObject, $oSecondRetrievedObject);
    }

    public function testCanUserUpdateObjectAndAddAttributes()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->value         = '123';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['id',     '=',  $oObject->id],
        ], false);
        $this->assertCount(1, $aItems);
        $oRetrievedObject = $aItems[0];
        $this->assertEquals($oObject, $oRetrievedObject);

        $oRetrievedObject->value = '345';
        $oRetrievedObject->addedAttribute = 'extra';
        $oSampleRepository->saveObject($oRetrievedObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['id',     '=',  $oObject->id],
        ], false);
        $this->assertCount(1, $aItems);
        $oSecondRetrievedObject = $aItems[0];
        $this->assertEquals($oRetrievedObject, $oSecondRetrievedObject);
    }

    public function testCanUserUpdateObjectAndRemoveAttributes()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->value         = '123';
        $oObject->addedAttribute = 'extra';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['id',     '=',  $oObject->id],
        ], false);
        $this->assertCount(1, $aItems);
        $oRetrievedObject = $aItems[0];
        $this->assertEquals($oObject, $oRetrievedObject);

        $oRetrievedObject->value = '345';
        unset($oRetrievedObject->addedAttribute);
        $oSampleRepository->saveObject($oRetrievedObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['id',     '=',  $oObject->id],
        ], false);
        $this->assertCount(1, $aItems);
        $oSecondRetrievedObject = $aItems[0];
        $this->assertEquals($oRetrievedObject, $oSecondRetrievedObject);
    }

    public function testCanUserDeleteObject()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->integer       = 123;
        $oObject->double        = 10.01;
        $oObject->string        = '100';
        $oSampleRepository->saveObject($oObject);

        $oSampleRepository->deleteObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([], false);
        $this->assertCount(0, $aItems);
    }

    public function testCanUserDeleteObjectUsingCompositeKeys()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', 'integer', [
            'id'            => 'string',
            'integer'       => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->integer       = 123;
        $oObject->double        = 10.01;
        $oObject->string        = '100';
        $oSampleRepository->saveObject($oObject);

        $oSampleRepository->deleteObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([], false);
        $this->assertCount(0, $aItems);
    }

    public function testAreNumbersCorrectlyConvertedWhenSavingAndRetrieving()
    {
        $oTableDefinition = $this->oTableManager->createTable('Sample_Repository', 'id', null, [
            'id'            => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oObject = new \stdClass;
        $oObject->id            = $this->oFaker->randomNumber(5);
        $oObject->integer       = 123;
        $oObject->double        = 10.01;
        $oObject->string        = '100';
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['id',     '=',  $oObject->id],
        ], false);

        $oRetrievedObject = $aItems[0];
        $this->assertEquals('integer', gettype($oRetrievedObject->integer)); 
        $this->assertEquals('double', gettype($oRetrievedObject->double)); 
        $this->assertEquals('string', gettype($oRetrievedObject->string)); 

    }

    public function testAreNoResultsReturnedWhenKeyIsQueryiedAsNull()
    {
        $oTableDefinition = $this->oTableManager->createTable('Sample_Repository', 'id', null, [
            'id'            => 'string',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['id',     '=',  null],
        ], false);
        $this->assertEquals([], $aItems);
    }

    public function testCanUserUseOperatorAliases() 
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'        => 'integer',
        ], []);
        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition);

        $oSampleRepository->setOperatorAliases(['EQ' => '=']);

        $oObject = (object) [
            'id'        => 123,
            'name'      => 'Test Name',
            'address'   => 'Test Address'
        ];
        
        $oSampleRepository->saveObject($oObject);

        $aItems = $oSampleRepository->getObjectsByANDCriteria([
            ['id',     'EQ',  $oObject->id],
        ], false);

        $this->assertEquals($oObject, $aItems[0]);
    }
}
