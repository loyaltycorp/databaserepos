<?php
use EoneoPay\DatabaseRepos\{Connection,TableManager,TableDefinition,IndexDefinition,Migration};

class TableManagementTest extends TestCase {

    public function testCanUserCreateListDeleteTables() 
    {
        $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'        => 'string',
        ], []);
        $aTables = $this->oTableManager->getListOfTables();
        $this->assertTrue(in_array($this->sTableName, $aTables));

        if($this->oTableManager->doesTableExist($this->sTableName)) {
            $this->oTableManager->deleteTable($this->sTableName);
        }

        $aTables = $this->oTableManager->getListOfTables();
        $this->assertFalse(in_array($this->sTableName, $aTables));
    }

    public function testIsATableDefinitionProperlyJsonEncoded()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
            'merchant_id'   => 'string',
            'amount'        => 'integer',
            'is_available'  => 'boolean',
            'name'          => 'string',
            'status'        => 'integer',
        ], [
            ['HashKey' => 'id', 'SortKey' => 'merchant_id']
        ]);
        $this->assertTrue($this->oTableManager->doesTableExist($this->sTableName));
        $this->oTableManager->deleteTable($this->sTableName);
        $sTableDefinitionJsonEncoded = json_encode($oTableDefinition, JSON_PRETTY_PRINT);

        $oTableDefinition = TableDefinition::createFromJson($sTableDefinitionJsonEncoded);
        $oTableDefinition = $this->oTableManager->createTableFromTableDefinition($oTableDefinition);

        $this->assertEquals($sTableDefinitionJsonEncoded, json_encode($oTableDefinition, JSON_PRETTY_PRINT));
    }

    public function testCanUserAddIndexAfterTableHasBeenCreated()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'pri_hash', null, [
            'pri_hash'            => 'string',
        ], [
        ]);

        $oTableDefinition->addAttribute('hash_key_index', 'string');
        // $oTableDefinition->addAttribute('sort_key_index', 'string');

        $oTableDefinition->addIndex(new IndexDefinition(
            'hash_key_index',
            null,
            'TestIndexName',
            'ALL',
            true,
            false,
            1
        ));

        $oOldTableDefinition = $this->oTableManager->getTableDefinitionFromExistingTable($this->sTableName);
        $oMigration = new Migration($oOldTableDefinition, $oTableDefinition);
        $this->assertFalse($this->oTableManager->hasIndexBeenCreated($oTableDefinition, 'TestIndexName'));
        $this->assertTrue($oMigration->execute($this->oTableManager));
        $this->assertTrue($this->oTableManager->hasIndexBeenCreated($oTableDefinition, 'TestIndexName'));
    }

    public function testCanUserDeleteIndexAfterItHasBeenCreated()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
            'merchant_id'   => 'string',
            'amount'        => 'integer',
            'is_available'  => 'boolean',
            'name'          => 'string',
            'status'        => 'integer',
        ], [
            ['HashKey' => 'id', 'SortKey' => 'merchant_id', 'IndexName' => 'TestIndex']
        ]);

        $oTableDefinition->deleteIndex('TestIndex');

        $oOldTableDefinition = $this->oTableManager->getTableDefinitionFromExistingTable($this->sTableName);
        $oMigration = new Migration($oOldTableDefinition, $oTableDefinition);
        $this->assertTrue($this->oTableManager->hasIndexBeenCreated($oTableDefinition, 'TestIndex'));
        $this->assertTrue($oMigration->execute($this->oTableManager));
        $this->assertFalse($this->oTableManager->hasIndexBeenCreated($oTableDefinition, 'TestIndex'));
    }

    public function testCanUserChangeAttributeTypeOfIndexAfterItHasBeenCreated()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'string',
            'merchant_id'   => 'string',
            'amount'        => 'integer',
            'is_available'  => 'boolean',
            'name'          => 'string',
            'status'        => 'integer',
        ], [
            ['HashKey' => 'id', 'SortKey' => 'status', 'IndexName' => 'TestIndex']
        ]);

        $oTableDefinition->addAttribute('status', 'string');

        $oOldTableDefinition = $this->oTableManager->getTableDefinitionFromExistingTable($this->sTableName);
        $oMigration = new Migration($oOldTableDefinition, $oTableDefinition);
        $this->assertTrue($oMigration->execute($this->oTableManager));
    }
}
