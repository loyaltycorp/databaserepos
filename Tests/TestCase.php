<?php
use Faker\Factory;
use EoneoPay\DatabaseRepos\{Connection,TableManager};

class TestCase extends \PHPUnit\Framework\TestCase 
{
    protected $oFaker;
    protected $oConnection;
    protected $oTableManager;
    protected $sTableName = 'Sample_Repository';

    public function __construct()
    {
        $this->oFaker = Factory::create();
        $this->oConnection = new Connection(
            getenv('AWS_DYNAMODB_ENDPOINT'),
            getenv('AWS_DYNAMODB_REGION'),
            getenv('AWS_DYNAMODB_VERSION'),
            getenv('AWS_DYNAMODB_ACCESS_KEY_ID'),
            getenv('AWS_DYNAMODB_SECRET_ACCESS_KEY')
        );
    }

    protected function setUp()
    {
        $this->oTableManager = new TableManager($this->oConnection);

        if($this->oTableManager->doesTableExist($this->sTableName)) {
            $this->oTableManager->deleteTable($this->sTableName);
        }
    }
}
