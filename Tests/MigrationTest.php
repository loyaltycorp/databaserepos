<?php
use EoneoPay\DatabaseRepos\{Connection,SampleRepository,TableManager,ANDCriteria,ORCriteria};

class MigrationTest extends TestCase 
{
    public function testDoesCreatingACollectionUsingAClosureWork()
    {
        $oTableDefinition = $this->oTableManager->createTable($this->sTableName, 'id', null, [
            'id'            => 'integer',
            'sortedNumber'  => 'integer',
            'sameName'      => 'string',

        ], [
            ['HashKey' => 'sameName', 'SortKey' => 'sortedNumber'],
        ]);

        $oCreateCollectionClosure = function(array $aItems) {
            return new ArrayIterator($aItems);
        };

        $oSampleRepository = new SampleRepository($this->oConnection, \stdClass::class, $oTableDefinition, $oCreateCollectionClosure);

        for ($i = 0; $i < 10; $i++) {
            $oObject = new \stdClass;
            $oObject->id            = $this->oFaker->randomNumber(5);
            $oObject->sortedNumber  = $this->oFaker->randomNumber(5);
            $oObject->sameName      = 'SameName';
            $oSampleRepository->saveObject($oObject);
        }

        $oArrayIterator = $oSampleRepository->getObjectsByANDCriteria([
            'sameName'  => 'SameName',
        ], true);

        $this->assertEquals(\ArrayIterator::class, get_class($oArrayIterator));
        $this->assertEquals(10, $oArrayIterator->count());
    }
}
