# README #

This readme documents how to configure, set up, and work with Repositories for DynamoDB.

### What is this repository for? ###

* Quick summary
Repositories for DynamoDB is a wrapper around the DynamoDB SDK for PHP.

* Version
0.1


### How do I get set up? ###

* Set up a connection to DynamoDB

```php
$oConnection = new Connection(
    getenv('AWS_DYNAMODB_ENDPOINT'),
    getenv('AWS_DYNAMODB_REGION'),
    getenv('AWS_DYNAMODB_VERSION'),
    getenv('AWS_DYNAMODB_ACCESS_KEY_ID'),
    getenv('AWS_DYNAMODB_SECRET_ACCESS_KEY')
);
```

* Initialize the TableManager

```php
$oTableManager = new TableManager($oConnection);
```

### How do I manage tables within DynamoDB ###

To create, list, delete and get information about tables, use the TableManager. The information
about a table is stored in a TableDefinition object. The TableDefinition specifies the primary key(s),
the fields which make up indexes, and the indexes themselves. This information is used whenever
data is queried to decide what index to use if any, datatypes to specify, type of operation to use (query
or scan) and the build-up of the query.

A TableDefinition object is returned whenever a table is created (useful when testing) or can be created 
on an existing table using the TableManager::getTableDefinitionFromExistingTable($sTableName) method.
This method queries DynamoDB which takes time. That's why it's recommended to serialize using json_encode()
and cache the JSON-representation of TableDefinition objects. 

#### Creating a Table ####

```php
$oTableDefinition = $oTableManager->createTable('TableName', 'LastName', 'FirstName', [
    'LastName'                => 'string', //primary hash key
    'FirstName'               => 'string', //primary sort key
], []);
```

#### Creating a Table with Global Indexes ####

```php
$oTableDefinition = $oTableManager->createTable('TableName', 'LastName', 'FirstName', [
    'LastName'                => 'string', //primary hash key
    'FirstName'               => 'string', //primary sort key
    'Address'                 => 'string', //index hash key
    'HouseNumber'             => 'integer', //index sort key
], [
    ['HashKey' => 'Address', 'SortKey' => 'HouseNumber'], //Defining index
    ... //more Indexes (to a max of 5 in total)
]);
```

#### Listing Tables ####

```php
$aTablesList = $oTableManager->getListOfTables();
```

#### Getting a TableDefinition object from an existing table ####

```php
$oTableDefinition = $oTableManager->getTableDefinitionFromExistingTable($tableName);
```

### How do I set up the Repository ###

To start querying for data, the repository has to be initialized using a 
Connection object and TableDefinition object. Also, the class has to be 
specified as which single objects are output.

```php
$oRepository = new Repository(
    $oConnection,
    \stdClass::class,
    $oTableDefinition
);
```

#### Querying a table ####

Once a Repository has been set up, it can be queried. This can be done using
the Repository::getObjectsByANDCriteria method. This method returns by default
an array of objects of the class as specified when constructing the Repository.

This method does not require parameters - if it has none it will return all 
objects in the table. The definition of the method is as follows:

```php
$oRepository->getObjectsByANDCriteria($aCriteria, 
                                    $bReturnBackwardsSorted,
                                    $iLimit, 
                                    $iOffset,
                                    $aNextVector);
```

The parameters will be discussed below.

##### Specifying Criteria #####
The array $aCriteria is an array of which the members can be strings, arrays or
objects which inherit the Criteria class (ANDCriteria and ORCriteria). The following
example shows which formats can be used. 

```php
$aCriteria = [
    'first_name' => 'Bob',
    ['first_name', '=', 'Bob'],
    ['first_name' => 'Bob'],
    new ORCriteria([
        ['first_name', '=', 'Bob'],
        ['first_name', '=', 'Alice'],
    ]),
];
```

If there is no operator specified, the operator used is 'equals' (=). The following operators
are available:

begins_with|contains|attribute_exists|attribute_not_exists|<=|>=|=|<|>|<>

If both a <= and >= operator are specified on a field, it will automatically be translated
to a BETWEEN operator. 

Please note that depending on the criteria and the available indexes, either a SCAN or QUERY 
operation will be chosen. See the DynamoDB documentation for more information on this topic.

##### Ascending or descending sorted #####

DynamoDB allows to return objects in either ascending or descending order - depending
on whether the parameter $bReturnBackwardsSorted is false or true. This function is only
available whenever a sort-key (either that of the index or the primary key) is used
within a Query-operation.

##### Limiting and Offsetting #####

To limit and offset a request, pass an integer as $iLimit and $iOffset. Because of the nature
of DynamoDB, whenever an offset is given, the entries offsetted are received but skipped.
The $aNextVector parameter allows to do a 'real' offsetting, although this feature is not 
(fully) implemented yet. Whenever this feature is implemented, the $aNextVector will require
the primary key of the last item of the last page, to do a real offset, rather than skipping
objects to offset.

#### Saving objects ####

Saving an object can be done using the Repository::saveObject method. This method's
only parameter is the object to be saved of the class as specified when constructing 
the Repository. It is required that an object has a value set for the primary hash key,
and if there is one, the primary sort key. If the object does not have either properties,
a SaveObjectException will be thrown.

```php
$oObject = (object) [
    'primary-hash-key' => 'Bob',
    'primary-sort-key' => 100,
];
$oRepository->saveObject($oObject);
```

#### Saving objects with Conditional Writes ####

If it is necessary to write an object only if a specific condition evaluates to true,
conditions can be specified as a parameter to the saveObject-method.

```php
$aConditions = [['id', 'attribute_not_exists', null]];
$oRepository->saveObject($oObject, true, $aConditions);
```

Above example will only write if ID does not exist. This example could be 
used to make sure this object won't overwrite a possibly existing object 
(only if one of the primary keys is set to 'id', and thereby a forced
field).

If the check fails (in above example, the attribute 'id' DOES exist), a 
ConditionalCheckFailedException will be thrown.

#### Deleting objects ####

Saving an object can be done using the Repository::deleteObject method. This method's
only parameter is the object to be saved of the class as specified when constructing 
the Repository. It is required that an object has a value set for the primary hash key,
and if there is one, the primary sort key. If the object does not have either properties,
a DeleteObjectException will be thrown.

#### Integrating saving objects within frameworks ####

If there are special requirements on what properties should or should not be saved, a
Closure can be specified to change this behaviour. This closure can be set upon 
cunstructor of the Repository or using the Repository::setPrepareToSaveClosure method.
This closure 

### How do I output other collection types of entities than arrays? ###

When a collection is requested, by default an array is returned.

Whenever other collection entities are desired, rather than arrays, use a closure which 
accepts an array of objects to create the desired collection entity.

```php
$oCreateCollectionClosure = function(array $aItems) {
    return new Collection($aItems); //Collection as in Laravel
};
$oRepository->setCreateCollectionClosure($oCreateCollectionClosure);
```

### How does the logging functionality work? ###

A logging library can be hooked up to this library using the Repository::setLoggingClosure
method. A closure passed to this method receives loglines while operating the Repository.
The closure can be used to filter on specific actions and tables. Below is an example given:

```php
$oRepository->setLoggingClosure(function($iLogLevel, $sAction, $sTableName, $sClassName, $sLogLine) {
    if ($iLogLevel != Repository::LOG_DEBUG) {
        \Log::error('[repository] ' . $sLogLine);
    }
});
```

### Not implemented yet / TODO ###

* Other index projection types than 'ALL'

* Local indexes

* Transactions

* Adding indexes to existing tables

* Operations: BatchGetItems, GetItem (improvement)

* Offsetting using vectors

* Querying non-scalar values

* Operator: IN, NOT

### Who do I talk to? ###

* Repo owner or admin
Vincent Oosterwijk - vincent@loyaltycorp.com.au


